<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

$logger = new Logger('klookva_logger');

$logger->pushHandler(new StreamHandler(APP_PATH . '/log/error_log.log', Logger::DEBUG));
$logger->pushHandler(new FirePHPHandler());