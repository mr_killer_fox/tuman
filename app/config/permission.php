<?php

use \Phalcon\Mvc\Dispatcher,
    \Phalcon\Events\Event,
    \Phalcon\Acl;

/**
 * Permission
 *
 * Prevents User Types from accessing areas they are not allowed in.
 */
class Permission extends \Phalcon\Mvc\User\Plugin {

    /**
     * Constants to prevent a typo
     */
    protected $roles;

    public function __construct()
    {
    	if (!Role::findFirstByname($this->config->roles->admin)){
    		$role_ad = new Role();
    		$role_ad->name = $this->config->roles->admin;
    		$role_ad->create();
    	}
    	if (!Role::findFirstByname($this->config->roles->user)){
    		$role_em = new Role();
    		$role_em->name = $this->config->roles->user;
    		$role_em->create();
    	}
    	if (!Role::findFirstByname($this->config->roles->guest)){
    		$role_gu = new Role();
    		$role_gu->name = $this->config->roles->guest;
    		$role_gu->create();
    	}

    	$this->roles = Role::find();
    }
    /**
     * Accessible to everyone
     * @var array
     */
    protected $_publicResources = [
        'index'  => ['index','signup','logout']
    ];

    /**
     * Accessible to Users (and Admins)
     * @var array
     */
    protected $_employeeResources = [
        'tabels' => ['*'],
        'orders' => ['*'],
        'products' => ['*'],
        'cocktails' => ['*'],
        'purchase' => ['*'],
        'dishes' => ['*'],
        'index'  => ['*']
    ];

    /**
     * Accessible to Admins
     * @var array
     */
    protected $_adminResources = [
        'role' => ['*'],
        'users' => ['*']
    ];

    // ------------------------------------------------------------------------

    /**
     * Triggers before a route is successfully executed
     *
     * @param  Event      $event
     * @param  Dispatcher $dispatcher
     *
     * @return boolean|void
     */

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {

        // Debug
        // $this->session->destroy();
        // Get the current role
        if (!$this->cookies->has('user-id')) {
            $role = $this->config->roles->guest;
        } else {
            $user_role_id = UserRole::minimum(
                array(
                    'user_id = :user_id:',
                    'bind' => array(
                        'user_id' => $this->cookies->get('user-id')
                    ),
                    "column" => "role_id"
                )
            );
            $role = Role::findFirstByRole_id($user_role_id)->name;
        }

        // Get the current Controller/Action from the dispatcher
        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();

        // Get the ACL Rule List
        $acl = $this->_getAcl();

        // See if they have permission
        $allowed = $acl->isAllowed($role, $controller, $action);
        if ($allowed != Acl::ALLOW)
        {
        	$this->flash->error('You do not have permission to access this area');
        	if ($acl->isAllowed($role, 'orders', 'index'))
            	$this->response->redirect('/../orders');
            else 
            	$this->response->redirect('/../index/signup');

            // Stop the dispatcher at the current operation
            $this->view->disable();
            return false;
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Build the Session ACL list one time if it's not set
     *
     * @return object
     */
    protected function _getAcl()
    {
      
        $acl = new Acl\Adapter\Memory();
        $acl->setDefaultAction(Acl::DENY);

        // Place all the roles inside the ACL Object
        foreach ($this->roles as $role) {
            $acl->addRole($role->name);
        }

        // Public Resources
        foreach ($this->_publicResources as $resource => $action) {
            $acl->addResource(new Acl\Resource($resource), $action);
        }

        // User Resources
        foreach ($this->_employeeResources as $resource => $action) {
            $acl->addResource(new Acl\Resource($resource), $action);
        }

        // Admin Resources
        foreach ($this->_adminResources as $resource => $action) {
            $acl->addResource(new Acl\Resource($resource), $action);
        }

        // Allow ALL Roles to access the Public Resources
        foreach ($this->roles as $role) {
            foreach($this->_publicResources as $resource => $actions) {
            	foreach ($actions as $action) {
                	$acl->allow($role->name, $resource, $action);
                }
            }
        }

        // Allow User & Admin to access the User Resources
        foreach ($this->_employeeResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow($this->config->roles->user, $resource, $action);
                $acl->allow($this->config->roles->admin, $resource, $action);
            }
        }

        // Allow Admin to access the Admin Resources
        foreach ($this->_adminResources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow($this->config->roles->admin, $resource, $action);
            }
        }



        $this->persistent->acl = $acl;

        return $this->persistent->acl;
    }

    // ------------------------------------------------------------------------

}