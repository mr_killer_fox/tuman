<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as Flash;

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
        'System' => APP_PATH . '/library/System/',
    ]
);
$loader->registerFiles(
    [
        APP_PATH . '/library/Composer/vendor/autoload.php',
    ]
);
$loader->register();


include APP_PATH . '/config/permission.php';
include APP_PATH . '/config/logger.php';
$config = include APP_PATH . "/config/config.php";
/**
 * Shared configuration service
 */
$di->setShared('config', function () use($config) {
    return $config;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'stat' => true,
              	'compileAlways' => true,
                'compiledSeparator' => '_'
            ]);

            $compiler = $volt->getCompiler();
                             
            $volt->getCompiler()->addFunction("mb_strimwidth", "mb_strimwidth");
            $volt->getCompiler()->addFunction("roundMe", "round");

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    $flash = new Flash([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
    $flash->setAutoescape(false);
    
   	return $flash;
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('crypt', function() use($config,$di) {
    $crypt = new \Phalcon\Crypt();
    $crypt->setKey($config->system_config->security_key);
    return $crypt;
});

$di->set('cookies', function () {
    $cookies = new \Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
}, true);

$scheme = \System\CommonOperations::getScheme();
$host = \System\CommonOperations::getHost();

$di->setShared('scheme',function () use ($scheme)
{
    return $scheme;
});

$di->setShared('host',function () use ($host)
{
    return $host;
});


$di->set('dispatcher', function() use ($di) {

    $eventsManager = $di->getShared('eventsManager');

    // Custom ACL Class
    $permission = new Permission();

    // Listen for events from the permission class

    $eventsManager->attach('dispatch', $permission);

    $eventsManager->attach("dispatch:beforeException",
        function($event, $dispatcher, $exception)
        {
            switch ($exception->getCode()) {
                case \Phalcon\Mvc\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Mvc\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(
                        array(
                            'controller' => 'index',
                            'action'     => 'signup',
                        )
                    );
                    return false;
            }
        }
    );

    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;

});

/**
 * Add Monolog logger
 */
$di->set('logger', function () use ($logger) {
    return $logger;
});