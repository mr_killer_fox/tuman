<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class CocktailsController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Коктели';
        $this->persistent->parameters = null;
        $cocktails = Cocktails::find(['order'=> 'name asc']);

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $cocktails,
            'limit'=> 200,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->title = 'Коктели';
        $this->view->h1 = 'Новый коктель';
        $this->view->products = Products::find(['order'=> 'name asc']);
    }

    /**
     * Edits a cocktail
     *
     * @param string $cocktail_id
     */
    public function editAction($cocktail_id)
    {
        if (!$this->request->isPost()) {

            $cocktail = Cocktails::findFirstBycocktail_id($cocktail_id);
            if (!$cocktail) {
                $this->flash->error("cocktail was not found");

                $this->dispatcher->forward([
                    'controller' => "cocktails",
                    'action' => 'index'
                ]);

                return;
            }
            $this->view->title = 'Коктели';
            $this->view->h1 = 'Изменить коктель '.$cocktail->name;
            $this->view->cocktail_id = $cocktail->cocktail_id;
            $this->view->products = Products::find(['order'=> 'name asc']);
            $this->view->cps = $cocktail->products;

            $this->tag->setDefault("cocktail_id", $cocktail->cocktail_id);
            $this->tag->setDefault("price", $cocktail->price);
            $this->tag->setDefault("name", $cocktail->name);
        }
    }

    /**
     * Creates a new cocktail
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'index'
            ]);

            return;
        }

        $cocktail = new Cocktails();
        $cocktail->price = $this->request->getPost("price");
        $cocktail->name = $this->request->getPost("name");

        if (!$cocktail->save()) {
            foreach ($cocktail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'new'
            ]);

            return;
        }

        $products = $this->request->getPost("product_id");
        $litters = $this->request->getPost("litter");
        foreach ($products as $product) {
            $cp = new CocktailProduct();
            $cp->cocktail_id = $cocktail->cocktail_id;
            $cp->product_id = $product;
            $cp->litter = $litters[$product];
            $cp->save();
        }

        $this->flash->success("cocktail was created successfully");

        $this->response->redirect('/../cocktails');
    }

    /**
     * Saves a cocktail edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'index'
            ]);

            return;
        }

        $cocktail_id = $this->request->getPost("cocktail_id");
        $cocktail = Cocktails::findFirstBycocktail_id($cocktail_id);

        if (!$cocktail) {
            $this->flash->error("cocktail does not exist " . $cocktail_id);

            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'index'
            ]);

            return;
        }

        $cocktail->price = $this->request->getPost("price");
        $cocktail->name = $this->request->getPost("name");

        if (!$cocktail->save()) {

            foreach ($cocktail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'edit',
                'params' => [$cocktail->cocktail_id]
            ]);

            return;
        }

        foreach ($cocktail->CocktailProduct as $cp) {
            $cp->delete();
        }

        $products = $this->request->getPost("product_id");
        $litters = $this->request->getPost("litter");
        foreach ($products as $product) {
            $cp = new CocktailProduct();
            $cp->cocktail_id = $cocktail->cocktail_id;
            $cp->product_id = $product;
            $cp->litter = $litters[$product];
            $cp->save();
        }

        $this->flash->success("cocktail was updated successfully");

        $this->response->redirect('/../cocktails');
    }

    /**
     * Deletes a cocktail
     *
     * @param string $cocktail_id
     */
    public function deleteAction($cocktail_id)
    {
        $cocktail = Cocktails::findFirstBycocktail_id($cocktail_id);
        if (!$cocktail) {
            $this->flash->error("cocktail was not found");

            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'index'
            ]);

            return;
        }

        foreach ($cocktail->CocktailProduct as $cp) {
            $cp->delete();
        }

        if (!$cocktail->delete()) {

            foreach ($cocktail->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "cocktails",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("cocktail was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "cocktails",
            'action' => "index"
        ]);
    }

}
