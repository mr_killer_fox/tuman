<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class DishesController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Посуда';
        $this->persistent->parameters = null;
        $dishes = Dishes::find();

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $dishes,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->title = 'Посуда';
        $this->view->h1 = 'Новая посуда';
    }

    /**
     * Edits a dishe
     *
     * @param string $dishe_id
     */
    public function editAction($dishe_id)
    {
        $this->view->title = 'Посуда';
        if (!$this->request->isPost()) {

            $dishe = Dishes::findFirstBydishe_id($dishe_id);
            if (!$dishe) {
                $this->flash->error("dishe was not found");

                $this->dispatcher->forward([
                    'controller' => "dishes",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->dishe_id = $dishe->dishe_id;
            $this->view->h1 = 'Обновление посуды '.$dishe->name;

            $this->tag->setDefault("dishe_id", $dishe->dishe_id);
            $this->tag->setDefault("name", $dishe->name);
            $this->tag->setDefault("price", $dishe->price);
            
        }
    }

    /**
     * Creates a new dishe
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'index'
            ]);

            return;
        }

        $dishe = new Dishes();
        $dishe->name = $this->request->getPost("name");
        $dishe->price = $this->request->getPost("price");
        

        if (!$dishe->save()) {
            foreach ($dishe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("dishe was created successfully");

        $this->response->redirect('/../dishes');
    }

    /**
     * Saves a dishe edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'index'
            ]);

            return;
        }

        $dishe_id = $this->request->getPost("dishe_id");
        $dishe = Dishes::findFirstBydishe_id($dishe_id);

        if (!$dishe) {
            $this->flash->error("dishe does not exist " . $dishe_id);

            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'index'
            ]);

            return;
        }

        $dishe->name = $this->request->getPost("name");
        $dishe->price = $this->request->getPost("price");
        

        if (!$dishe->save()) {

            foreach ($dishe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'edit',
                'params' => [$dishe->dishe_id]
            ]);

            return;
        }

        $this->flash->success("dishe was updated successfully");

        $this->response->redirect('/../dishes');
    }

    /**
     * Deletes a dishe
     *
     * @param string $dishe_id
     */
    public function deleteAction($dishe_id)
    {
        $dishe = Dishes::findFirstBydishe_id($dishe_id);
        if (!$dishe) {
            $this->flash->error("dishe was not found");

            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'index'
            ]);

            return;
        }

        if (!$dishe->delete()) {

            foreach ($dishe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "dishes",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("dishe was deleted successfully");

        $this->response->redirect('/../dishes');
    }

}
