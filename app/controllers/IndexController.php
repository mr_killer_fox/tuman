<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
    	$this->view->title = 'kLooKva admin panel';
		$this->dispatcher->forward(
		    [
		        'controller' => 'orders',
				'action' => 'index'
		    ]
		);
    }

    private function getIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function totalAction()
    {
        $this->view->title = 'Итоги';
        $years = $this->request->getPost('date') ? date('Y', strtotime('01-'.$this->request->getPost('date'))) : date('Y');
        $month = $this->request->getPost('date') ? date('m', strtotime('01-'.$this->request->getPost('date'))) : date('m');
        $hours = $this->config->system_config->hour_end.':00:00';
        $days = $this->config->system_config->day_end;
        $date_from = $years.'-'.$month.'-'.$days.' '.$hours;
        $date_to = date('Y-m-d H:i:s', strtotime($date_from.'+ 1 month'));
        $orders = Orders::find(
            array(
                'conditions' => 'active = 0 AND date_upd > :date_from: AND date_upd < :date_to:',
                'bind' => array(
                    'date_from' => $date_from,
                    'date_to' => $date_to
                ),
                'order' => 'date_upd DESC,order_id DESC'
            )
        );

        //$products = Products::find();
        $purchases = Purchase::find(
            array(
                'conditions' => 'is_buy = 1 AND date_add > :date_from: AND date_add < :date_to:',
                'bind' => array(
                    'date_from' => $date_from,
                    'date_to' => $date_to
                ),
                'order' => 'date_add DESC,purchase_id DESC'
            )
        );
        $total_buy = 0;
        $total_sell = 0;

        foreach($purchases as $purchase) {
            $total_buy += $purchase->price * $purchase->count;
        }
        $products = array();
        foreach($orders as $order) {
            foreach ($order->positions as $position) {
                if ($position->type_name == 'cocktail' || $position->type_name == 'cocktail_single') {
                    $cocktail = Cocktails::findFirst($position->type_id);
                    foreach($cocktail->products as $product) {
                        if (array_key_exists($product->product_id, $products)) {
                            $products[$product->product_id]['litter'] += $product->litter * $position->count;
                            $products[$product->product_id]['price'] += $product->price * $position->count;
                        } else {
                            $products[$product->product_id]['litter'] = $product->litter * $position->count;
                            $products[$product->product_id]['price'] = $product->price * $position->count;
                            $products[$product->product_id]['name'] = $product->name;
                        }
                    }
                }
            }
            $total_sell += $order->total_true_price;
        }

        $this->view->total_buy = $total_buy;
        $this->view->total_sell = $total_sell;
        $this->view->products = $products;
        $this->view->datepick = date('m-Y', strtotime($date_from));
    }


    
    public function signupAction()
    {
    	$this->view->title = 'Туман бар';

    	if ($this->request->getPost('signup') != 1)
    		return;
    	$email = $this->request->getPost('email');
    	$password = $this->request->getPost('password');
    	$user = Users::findFirst(['conditions' => "email='".$email."'"]);
    	if ($user) {
            if ($this->security->checkHash($password, $user->password)) {
            	$this->cookies->set('user-id', $user->user_id);
                $this->cookies->send();
                $this->logger->info('User '.$user->name.' login with ip:'.$this->getIp());
				$this->response->redirect('/../orders');
            } else {
            	$this->flash->error("Incorect login or password");
                $this->logger->warning("Incorect login or password");
            }
        } else {
        	$this->flash->error("Incorect login or password");
            $this->logger->warning("Incorect login or password");
            $this->security->hash(rand());
        }
    }

    public function logoutAction()
    {
    	$this->session->destroy();
        $this->cookies->get("user-id")->delete();
		$this->response->redirect('/../');
    }
}

