<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class OrdersController extends ControllerBase
{
    public $groups = array(
        'cocktail',
        'cocktail_single',
        'dishe'
    );
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Заказы';
        $this->persistent->parameters = null;
        $orders = Orders::find(
            array(
                'conditions' => 'active = 1',
                'order' => 'date_upd DESC,order_id DESC'
            )
        );

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $orders,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    public function closedAction()
    {
        $this->view->title = 'Заказы закрытые';
        $this->persistent->parameters = null;
        $hours = $this->config->system_config->hour_end.':00:00';
        $date_from = $this->request->getPost('date') ? date('Y-m-d', strtotime($this->request->getPost('date'))) : date('Y-m-d');
        $date_to = $this->request->getPost('date') ? date('Y-m-d', strtotime($this->request->getPost('date').'+ 1 day')) : date('Y-m-d', strtotime(date('Y-m-d').'+ 1 day'));
        $orders = Orders::find(
            array(
                'conditions' => 'active = 0 AND date_upd > :date_from: AND date_upd < :date_to:',
                'bind' => array(
                    'date_from' => $date_from.' '.$hours,
                    'date_to' => $date_to.' '.$hours
                ),
                'order' => 'date_upd DESC,order_id DESC'
            )
        );

        $total_price_closed = 0;

        foreach ($orders as $order) {
            $total_price_closed += $order->total_price;
        }

        $this->view->total_price_closed = $total_price_closed;

        $this->view->datepick = date('m/d/Y', strtotime($date_from));
        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $orders,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }


    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->title = 'Заказы';
        $this->view->h1 = 'Новый заказ';
        $this->view->tabels = Tabels::find();
        //$this->view->hookans = Hookans::find(['order'=> 'name asc']);
        $this->view->cocktails = Cocktails::find(['order'=> 'name asc']);
        $this->view->dishes = Dishes::find(['order'=> 'name asc']);
    }

    /**
     * Edits a order
     *
     * @param string $order_id
     */
    public function editAction($order_id)
    {
        $this->view->title = 'Заказы';
        if (!$this->request->isPost()) {
            $this->view->h1 = 'Изменить заказ №'.$order_id;
            $order = Orders::findFirstByorder_id($order_id);
            if (!$order) {
                $this->flash->error("order was not found");

                $this->dispatcher->forward([
                    'controller' => "orders",
                    'action' => 'index'
                ]);

                return;
            }
            if ($order->active != 1) {
                $this->flash->error("Заказ закрыт");

                $this->dispatcher->forward([
                    'controller' => "orders",
                    'action' => 'index'
                ]);

                return;
            }
            $this->view->order_id = $order->order_id;
            $this->view->tabels = Tabels::find();
            //$this->view->hookans = Hookans::find(['order'=> 'name asc']);
            $this->view->cocktails = Cocktails::find(['order'=> 'name asc']);
            $this->view->dishes = Dishes::find(['order'=> 'name asc']);
            
            $this->tag->setDefault("order_id", $order->order_id);
            $this->tag->setDefault("comment", $order->comment);
            $this->tag->setDefault("total_price", $order->total_price);
            $this->tag->setDefault("price", $order->total_true_price);

            $this->view->tabel_id = $order->tabel_id;
            $this->view->positions = $order->positions;
        }
    }

    /**
     * Creates a new order
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'index'
            ]);

            return;
        }

        $order = new Orders();
        $order->tabel_id = $this->request->getPost("tabel_id");
        $order->comment = $this->request->getPost("comment");
        $order->user_id = $this->cookies->get("user-id")->getValue();
        $order->date_add = date('Y-m-d H:i:s');
        $order->date_upd = date('Y-m-d H:i:s');
        $order->active = 1;
        
        if (!$order->save()) {
            foreach ($order->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'new'
            ]);

            return;
        }

        foreach ($this->groups as $group)
        {
            if ($this->request->getPost('price_'.$group)) {
                foreach ($this->request->getPost('price_'.$group) as $id => $price) {
                    $position = new Positions();
                    $position->order_id = $order->order_id;
                    $position->type_id = $id;
                    $position->type_name = $group;
                    $position->price = $price;
                    $position->count = $this->request->getPost('count_'.$group)[$id];
                    $position->save();
                }
            }
        }

        $this->flash->success("order was created successfully");

        $this->response->redirect('/../orders');
    }

    /**
     * Saves a order edited
     *
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'index'
            ]);

            return;
        }

        $order_id = $this->request->getPost("order_id");
        $order = Orders::findFirstByorder_id($order_id);

        if (!$order) {
            $this->flash->error("order does not exist " . $order_id);

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'index'
            ]);

            return;
        }

        $order->tabel_id = $this->request->getPost("tabel_id");
        $order->user_id = $this->cookies->get("user-id")->getValue();
        $order->date_upd = date('Y-m-d H:i:s');
        

        if (!$order->save()) {

            foreach ($order->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'edit',
                'params' => [$order->order_id]
            ]);

            return;
        }
        foreach ($order->Positions as $position) {
            $position->delete();
        }
        foreach ($this->groups as $group)
        {
            if ($this->request->getPost('price_'.$group)) {
                foreach ($this->request->getPost('price_'.$group) as $id => $price) {
                    $position = new Positions();
                    $position->order_id = $order->order_id;
                    $position->type_id = $id;
                    $position->type_name = $group;
                    $position->price = $price;
                    $position->count = $this->request->getPost('count_'.$group)[$id];
                    $position->save();
                }
            }
        }

        $this->flash->success("order was updated successfully");

        $this->response->redirect('/../orders');
    }

    /**
     * Deletes a order
     *
     * @param string $order_id
     */
    public function deleteAction($order_id)
    {
        $order = Orders::findFirstByorder_id($order_id);
        if (!$order) {
            $this->flash->error("order was not found");

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'index'
            ]);

            return;
        }

        $order->active = 99;

        if (!$order->save()) {

            foreach ($order->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'delete',
                'params' => [$order->order_id]
            ]);

            return;
        }

        // if (!$order->delete()) {

        //     foreach ($order->getMessages() as $message) {
        //         $this->flash->error($message);
        //     }

        //     $this->dispatcher->forward([
        //         'controller' => "orders",
        //         'action' => 'search'
        //     ]);

        //     return;
        // }

        $this->flash->success("order was deleted successfully");

        $this->response->redirect('/../orders/closed');
    }

    public function closeorderAction($order_id = null)
    {
        $order = false;
        if ($order_id) {
            $order = Orders::findFirstByorder_id($order_id);
            
        } elseif ($this->request->isPost()) {
            $order = new Orders();
            $order->date_add = date('Y-m-d H:i:s');
        }

        if (!$order) {
            $this->flash->error("order was not found");

            $this->dispatcher->forward([
                'controller' => "orders",
                'action' => 'index'
            ]);

            return;
        }
        

        $order->active = 0;
        $order->date_upd = date('Y-m-d H:i:s');

        if (!$this->request->isPost()) {
            if (!$order->save()) {
                foreach ($order->getMessages() as $message) {
                    $this->flash->error($message);
                }

                $this->dispatcher->forward([
                    'controller' => "orders",
                    'action' => 'closeorder',
                    'params' => [$order->order_id]
                ]);

                return;
            }
        }

        if ($this->request->isPost()) {
            $order->tabel_id = $this->request->getPost("tabel_id");
            $order->user_id = $this->cookies->get("user-id")->getValue();

            if (!$order->save()) {

                foreach ($order->getMessages() as $message) {
                    $this->flash->error($message);
                }

                $this->dispatcher->forward([
                    'controller' => "orders",
                    'action' => 'edit',
                    'params' => [$order->order_id]
                ]);

                return;
            }
            foreach ($order->Positions as $position) {
                $position->delete();
            }
            foreach ($this->groups as $group)
            {
                if ($this->request->getPost('price_'.$group)) {
                    foreach ($this->request->getPost('price_'.$group) as $id => $price) {
                        $position = new Positions();
                        $position->order_id = $order->order_id;
                        $position->type_id = $id;
                        $position->type_name = $group;
                        $position->price = $price;
                        $position->count = $this->request->getPost('count_'.$group)[$id];
                        $position->save();
                    }
                }
            }
        }

        $this->flash->success("order was closed successfully");

        $this->response->redirect('/../orders');
    }

}
