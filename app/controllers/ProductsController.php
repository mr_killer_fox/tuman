<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class ProductsController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Ингредиенты';
        $this->persistent->parameters = null;
        $products = Products::find();

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $products,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->title = 'Ингредиенты';
        $this->view->h1 = 'Новый ингредиент';
    }

    /**
     * Edits a product
     *
     * @param string $product_id
     */
    public function editAction($product_id)
    {
        $this->view->title = 'Ингредиенты';
        if (!$this->request->isPost()) {

            $product = Products::findFirstByproduct_id($product_id);
            if (!$product) {
                $this->flash->error("product was not found");

                $this->dispatcher->forward([
                    'controller' => "products",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->product_id = $product->product_id;
            $this->view->h1 = 'Обновление ингредиента '.$product->name;

            $this->tag->setDefault("product_id", $product->product_id);
            $this->tag->setDefault("name", $product->name);
            $this->tag->setDefault("litter", $product->litter);
            $this->tag->setDefault("price", $product->price);
            
        }
    }

    /**
     * Creates a new product
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'index'
            ]);

            return;
        }

        $product = new Products();
        $product->name = $this->request->getPost("name");
        $product->litter = $this->request->getPost("litter");
        $product->price = $this->request->getPost("price");
        

        if (!$product->save()) {
            foreach ($product->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("product was created successfully");

        $this->response->redirect('/../products');
    }

    /**
     * Saves a product edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'index'
            ]);

            return;
        }

        $product_id = $this->request->getPost("product_id");
        $product = Products::findFirstByproduct_id($product_id);

        if (!$product) {
            $this->flash->error("product does not exist " . $product_id);

            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'index'
            ]);

            return;
        }

        $product->name = $this->request->getPost("name");
        $product->litter = $this->request->getPost("litter");
        $product->price = $this->request->getPost("price");
        

        if (!$product->save()) {

            foreach ($product->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'edit',
                'params' => [$product->product_id]
            ]);

            return;
        }

        $this->flash->success("product was updated successfully");

        $this->response->redirect('/../products');
    }

    /**
     * Deletes a product
     *
     * @param string $product_id
     */
    public function deleteAction($product_id)
    {
        $product = Products::findFirstByproduct_id($product_id);
        if (!$product) {
            $this->flash->error("product was not found");

            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'index'
            ]);

            return;
        }

        if (!$product->delete()) {

            foreach ($product->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "products",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("product was deleted successfully");

       $this->response->redirect('/../products');
    }

}
