<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class PurchaseController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Закупки';
        $this->persistent->parameters = null;
        $years = $this->request->getPost('date') ? date('Y', strtotime('01-'.$this->request->getPost('date'))) : date('Y');
        $month = $this->request->getPost('date') ? date('m', strtotime('01-'.$this->request->getPost('date'))) : date('m');
        $hours = $this->config->system_config->hour_end.':00:00';
        $days = $this->config->system_config->day_end;
        $date_from = $years.'-'.$month.'-'.$days.' '.$hours;

        if ($this->request->getPost('date')) {
            $this->session->set('date_purchase', $date_from);
        }
        $date_from = $this->session->has('date_purchase') ? $this->session->get('date_purchase') : $date_from;
        $date_to = date('Y-m-d H:i:s', strtotime($date_from.'+ 1 month'));
        $this->view->datepick = date('m-Y', strtotime($date_from));
        $purchase = Purchase::find(
            array(
                'conditions' => 'date_add > :date_from: AND date_add < :date_to:',
                'bind' => array(
                    'date_from' => $date_from.' '.$hours,
                    'date_to' => $date_to.' '.$hours
                ),
                'order' => 'is_buy DESC,date_add DESC,purchase_id DESC'
            )
        );

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $purchase,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->title = 'Закупки';
        $this->view->h1 = 'Новая позиция';
        $this->view->products = Products::find();
    }

    /**
     * Edits a purchase
     *
     * @param string $purchase_id
     */
    public function editAction($purchase_id)
    {
        $this->view->title = 'Закупки';
        $this->view->products = Products::find();
        if (!$this->request->isPost()) {

            $purchase = Purchase::findFirstBypurchase_id($purchase_id);
            if (!$purchase) {
                $this->flash->error("purchase was not found");

                $this->dispatcher->forward([
                    'controller' => "purchase",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->purchase_id = $purchase->purchase_id;
            $this->view->h1 = 'Обновление позиции '.$purchase->name;

            $this->tag->setDefault("purchase_id", $purchase->purchase_id);
            $this->view->product_id = $purchase->product_id ?? 0;
            $this->view->is_buy = $purchase->is_buy;
            $this->tag->setDefault("name", $purchase->name);
            $this->tag->setDefault("price", $purchase->price);
            $this->tag->setDefault("count", $purchase->count);
        }
    }

    public function buyAction($id, $is_buy, $page = 1)
    {
        $purchase = Purchase::findFirstBypurchase_id($id);

        if (!$purchase) {
            $this->flash->error("purchase does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'index'
            ]);

            return;
        }

        $purchase->is_buy = $is_buy;
        $purchase->date_add = date('Y-m-d H:i:s');
        if (!$purchase->save()) {
            foreach ($purchase->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("purchase was update successfully");

        $this->response->redirect('/../purchase?page='.$page);
    }

    /**
     * Creates a new purchase
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'index'
            ]);

            return;
        }

        $purchase = new Purchase();
        
        if ($this->request->getPost("product_id") == 0) {
            $name = $this->request->getPost("name");
            $price = $this->request->getPost("price");
            
            if (!$name || !$price) {
                $this->flash->error("Add price or name to purchase");
                $this->dispatcher->forward([
                    'controller' => "purchase",
                    'action' => 'new'
                ]);
                return;
            }
            $purchase->name = $name;
            $purchase->price = $price;
        } else {
            $purchase->product_id = $this->request->getPost("product_id");
        }

        $purchase->count = $this->request->getPost("count");
        $purchase->date_add = date('Y-m-d H:i:s');
        $purchase->is_buy = $this->request->getPost("is_buy");
        
        if (!$purchase->save()) {
            foreach ($purchase->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("purchase was created successfully");

        $this->response->redirect('/../purchase');
    }

    /**
     * Saves a purchase edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'index'
            ]);

            return;
        }

        $purchase_id = $this->request->getPost("purchase_id");
        $purchase = Purchase::findFirstBypurchase_id($purchase_id);

        if (!$purchase) {
            $this->flash->error("purchase does not exist " . $purchase_id);

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'index'
            ]);

            return;
        }

        if ($this->request->getPost("product_id") == 0) {
            $name = $this->request->getPost("name");
            $price = $this->request->getPost("price");
            
            if (!$name || !$price) {
                $this->flash->error("Add price or name to purchase");
                $this->dispatcher->forward([
                    'controller' => "purchase",
                    'action' => 'new'
                ]);
                return;
            }
            $purchase->name = $name;
            $purchase->price = $price;
        } else {
            $purchase->product_id = $this->request->getPost("product_id");
        }

        $purchase->count = $this->request->getPost("count");
        $purchase->date_add = date('Y-m-d H:i:s');
        

        if (!$purchase->save()) {

            foreach ($purchase->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'edit',
                'params' => [$purchase->purchase_id]
            ]);

            return;
        }

        $this->flash->success("purchase was updated successfully");

        $this->response->redirect('/../purchase');
    }

    /**
     * Deletes a purchase
     *
     * @param string $purchase_id
     */
    public function deleteAction($purchase_id)
    {
        $purchase = Purchase::findFirstBypurchase_id($purchase_id);
        if (!$purchase) {
            $this->flash->error("purchase was not found");

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'index'
            ]);

            return;
        }

        if (!$purchase->delete()) {

            foreach ($purchase->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "purchase",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("purchase was deleted successfully");

        $this->response->redirect('/../purchase');
    }

}
