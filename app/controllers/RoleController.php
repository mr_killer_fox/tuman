<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class RoleController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Roles';
        $this->persistent->parameters = null;
        $role = Role::find();
        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $role,
            'limit'=> 10,
            'page' => $currentPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
    	$this->view->title = 'Roles';
    	$this->view->h1 = 'New role';
    }

    /**
     * Edits a role
     *
     * @param string $role_id
     */
    public function editAction($role_id)
    {
    	$this->view->title = 'Roles';
        if (!$this->request->isPost()) {

            $role = Role::findFirstByrole_id($role_id);
            if (!$role) {
                $this->flash->error("role was not found");

                $this->logger->warning("role was not found");

                $this->dispatcher->forward([
                    'controller' => "role",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->role_id = $role->role_id;
            $this->view->h1 = 'Update role '.$role->name;

            $this->tag->setDefault("role_id", $role->role_id);
            $this->tag->setDefault("name", $role->name);
            
        }
    }

    /**
     * Creates a new role
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'index'
            ]);

            return;
        }

        $role = new Role();
        $role->roleId = $this->request->getPost("role_id");
        $role->name = $this->request->getPost("name");
        

        if (!$role->save()) {
            foreach ($role->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->logger->error(implode(', ', $role->getMessages()));

            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("role was created successfully");

        $this->dispatcher->forward([
            'controller' => "role",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a role edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'index'
            ]);

            return;
        }

        $role_id = $this->request->getPost("role_id");
        $role = Role::findFirstByrole_id($role_id);

        if (!$role) {
            $this->flash->error("role does not exist " . $role_id);

            $this->logger->warning("role does not exist " . $role_id);

            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'index'
            ]);

            return;
        }

        $role->roleId = $this->request->getPost("role_id");
        $role->name = $this->request->getPost("name");
        

        if (!$role->save()) {

            foreach ($role->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->logger->error(implode(', ', $role->getMessages()));

            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'edit',
                'params' => [$role->role_id]
            ]);

            return;
        }

        $this->flash->success("role was updated successfully");

        $this->dispatcher->forward([
            'controller' => "role",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a role
     *
     * @param string $role_id
     */
    public function deleteAction($role_id)
    {
        $role = Role::findFirstByrole_id($role_id);
        if (!$role) {
            $this->flash->error("role was not found");

            $this->logger->warning("role was not found");

            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'index'
            ]);

            return;
        }

        if (!$role->delete()) {

            foreach ($role->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->logger->error(implode(', ', $role->getMessages()));

            $this->dispatcher->forward([
                'controller' => "role",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("role was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "role",
            'action' => "index"
        ]);
    }

}
