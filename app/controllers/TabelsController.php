<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class TabelsController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Столы';
        $this->persistent->parameters = null;
        $tabels = Tabels::find();

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new Paginator([
            'data' => $tabels,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Creates a new tabel
     */
    public function createAction()
    {
        $tabel = new Tabels();
        $tabel->is_locked = 0;
        if (!$tabel->create()) {
            foreach ($tabel->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "tabels",
                'action' => 'index'
            ]);

            return;
        }

        $this->flash->success("tabel was created successfully");

        $this->response->redirect('/../tabels');
    }

    /**
     * Deletes a tabel
     *
     * @param string $tabel_id
     */
    public function deleteAction($tabel_id)
    {
        $tabel = Tabels::findFirstBytabel_id($tabel_id);
        if (!$tabel) {
            $this->flash->error("tabel was not found");

            $this->dispatcher->forward([
                'controller' => "tabels",
                'action' => 'index'
            ]);

            return;
        }

        if (!$tabel->delete()) {

            foreach ($tabel->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->response->redirect('/../tabels');

            return;
        }

        $this->flash->success("tabel was deleted successfully");

        $this->response->redirect('/../tabels');
    }

}
