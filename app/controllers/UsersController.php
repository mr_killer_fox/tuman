<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class UsersController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->title = 'Пользователи';
        $this->persistent->parameters = null;
        $user = Users::find(null, false);
        $user_cl = [];
        foreach ($user as $key => $user_one) {
			$user_cl[$key] = $user_one;
        }

        $currentPage = (int) $this->request->getQuery('page') ?? 1;
        $paginator = new PaginatorArray([
            'data' => $user_cl,
            'limit'=> 10,
            'page' => $currentPage
        ]);
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
    	$this->view->title = 'Пользователи';
    	$this->view->h1 = 'New user';
    	$this->view->roles = Role::find();
    }

    /**
     * Edits a user
     *
     * @param string $user_id
     */
    public function editAction($user_id)
    {
    	$this->view->title = 'Пользователи';
        if (!$this->request->isPost()) {

            $user = Users::findFirst([
                'user_id = :user_id:',
                'bind' => [
                    'user_id' => $user_id
                ]
            ], false);
            if (!$user) {
                $this->flash->error("user was not found");

                $this->logger->warning("user was not found");

                $this->dispatcher->forward([
                    'controller' => "users",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->user_id = $user->user_id;
            $this->view->h1 = 'Изменить пользователя '.$user->name;
    		$this->view->roles = Role::find();
    		$select_roles = UserRole::findByuser_id($user_id) ? UserRole::findByuser_id($user_id) : new UserRole;
    		$need_role = [];
    		foreach ($select_roles as $select_role)
    			$need_role[] = $select_role->role_id;
    		$this->view->select_role = $need_role;
            $this->tag->setDefault("user_id", $user->user_id);
            $this->tag->setDefault("name", $user->name);
            $this->tag->setDefault("email", $user->email);
            $this->view->user_active = $user->active;
            
        }
    }

    /**
     * Creates a new user
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        $user = new Users();
        $user->name = $this->request->getPost("name");
        $user->email = $this->request->getPost("email", "email");
        $user->password = $this->security->hash($this->request->getPost("password"));
        $user->active = ($this->request->getPost("active") == 'on') ? 1 : 0;

        if (!$user->save()) {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'new'
            ]);

            return;
        }
		
		foreach($this->request->getPost("user_role") as $role) {
			$user_role = new UserRole();
			$user_role->user_id = $user->user_id;
			$user_role->role_id = $role;
			$user_role->save();
		}
 		$user_group->save();

        $this->flash->success("user was created successfully");

        $this->response->redirect('/../users');
    }

    /**
     * Saves a user edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        $user_id = $this->request->getPost("user_id");
        $user = Users::findFirst([
            'user_id = :user_id:',
            'bind' => [
                'user_id' => $user_id
            ]
        ], false);

        if (!$user) {
            $this->flash->error("user does not exist " . $user_id);

            $this->logger->warning("user does not exist " . $user_id);

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }

        $user->name = $this->request->getPost("name");
        $user->email = $this->request->getPost("email", "email");
        $user->active = ($this->request->getPost("active") == 'on') ? 1 : 0;
        if ( trim($this->request->getPost("password")) != '')
    		$user->password = $this->security->hash($this->request->getPost("password"));
        
        $user_roles = UserRole::findByuser_id($user_id);
        foreach($user_roles as $user_role) {
        	$user_role->delete();
        }
        foreach($this->request->getPost("user_role") as $role) {
			$user_role = new UserRole();
			$user_role->user_id = $user_id;
			$user_role->role_id = $role;
			$user_role->save();
		}
        if (!$user->save()) {

            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->logger->error(implode(', ', $user->getMessages()));

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'edit',
                'params' => [$user->user_id]
            ]);

            return;
        }

        $this->flash->success("user was updated successfully");

        $this->response->redirect('/../users');
    }

    /**
     * Deletes a user
     *
     * @param string $user_id
     */
    public function deleteAction($user_id)
    {
        $user = Users::findFirst([
            'user_id = :user_id:',
            'bind' => [
                'user_id' => $user_id
            ]
        ], false);
        $user_role = UserRole::findFirstByuser_id($user_id);
        if (!$user) {
            $this->flash->error("user was not found");

            $this->logger->warning("user was not found");

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'index'
            ]);

            return;
        }
        
        if ($user_role)
            $user_role->delete();

        if (!$user->delete()) {

            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->logger->error(implode(', ', $user->getMessages()));

            $this->dispatcher->forward([
                'controller' => "users",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("user was deleted successfully");

        $this->response->redirect('/../users');
    }

}
