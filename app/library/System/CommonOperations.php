<?php 
namespace System;
/**
 * Base class to retrieve data For Masternode
 */
class CommonOperations
{
    public static function getScheme()
    {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        return $isSecure ? 'https' : 'http';
    }

    public static function getHost()
    {
        return (!empty($_SERVER['HTTP_X_ORIGINAL_HOST']) && !is_null($_SERVER['HTTP_X_ORIGINAL_HOST'])) ? $_SERVER['HTTP_X_ORIGINAL_HOST'] : $_SERVER['HTTP_HOST'];
    }
}