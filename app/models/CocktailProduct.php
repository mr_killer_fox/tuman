<?php

class CocktailProduct extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="cocktail_id", type="integer", length=10, nullable=false)
     */
    public $cocktail_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="product_id", type="integer", length=10, nullable=false)
     */
    public $product_id;

    /**
     *
     * @var double
     * @Column(column="litter", type="double", nullable=false)
     */
    public $litter;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("cocktail_product");
        $this->belongsTo('cocktail_id', '\Cocktails', 'cocktail_id', ['alias' => 'Cocktails']);
        $this->belongsTo('product_id', '\Products', 'product_id', ['alias' => 'Products']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cocktail_product';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CocktailProduct[]|CocktailProduct|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CocktailProduct|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
