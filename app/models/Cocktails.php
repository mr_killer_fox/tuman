<?php

class Cocktails extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="cocktail_id", type="integer", length=10, nullable=false)
     */
    public $cocktail_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", nullable=false)
     */
    public $name;

    /**
     *
     * @var double
     * @Column(column="price", type="double", nullable=false)
     */
    public $price;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("cocktails");
        $this->hasMany('cocktail_id', 'CocktailProduct', 'cocktail_id', ['alias' => 'CocktailProduct']);
    }

    public function afterFetch()
    {
        $cps = CocktailProduct::findBycocktail_id($this->cocktail_id);
        $this->products = '';
        if (count($cps) > 0) {
            $litter = 0;
            $price = 0;
            $this->products = array();
            foreach ($cps as $key => $cp) {
                $litter += $cp->litter;
                $product = Products::findFirst($cp->product_id);
                $cps[$key]->price = $cp->litter * $product->price / $product->litter;
                $cps[$key]->name = $product->name;
                $price += $cps[$key]->price;
                $this->products[$key] = $cps[$key];
            }
            $this->total_litter = $litter;
            $this->total_price = $price;
        }
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cocktails';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cocktails[]|Cocktails|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cocktails|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
