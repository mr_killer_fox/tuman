<?php

class Orders extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="order_id", type="integer", length=10, nullable=false)
     */
    public $order_id;

    /**
     *
     * @var integer
     * @Column(column="tabel_id", type="integer", length=10, nullable=false)
     */
    public $tabel_id;

    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=10, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(column="date_add", type="string", nullable=false)
     */
    public $date_add;

    /**
     *
     * @var string
     * @Column(column="date_upd", type="string", nullable=false)
     */
    public $date_upd;

    /**
     *
     * @var string
     * @Column(column="comment", type="string")
     */
    public $comment;

    /**
     *
     * @var integer
     * @Column(column="active", type="integer", length=1, nullable=false)
     */
    public $active;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("orders");
        $this->hasMany('order_id', 'Positions', 'order_id', ['alias' => 'Positions']);
        $this->belongsTo('tabel_id', '\Tabels', 'tabel_id', ['alias' => 'Tabels']);
        $this->belongsTo('user_id', '\Users', 'user_id', ['alias' => 'Users']);
    }

    public function afterFetch()
    {
        $positions = Positions::findByorder_id($this->order_id);
        $this->positions = '';
        if (count($positions) > 0) {
            $price = 0;
            $true_price = 0;
            $this->positions = array();
            foreach ($positions as $key => $position) {
                $price += $position->price * $position->count;
                if ($position->type_name == 'cocktail_single') {
                    $class_name = 'Cocktails';
                } else {
                    $class_name = ucfirst($position->type_name).'s';
                }
                $pos = $class_name::findFirst($position->type_id);
                if (!$pos) {
                    continue;
                }
                $positions[$key]->true_price = $pos->price * $position->count;
                $positions[$key]->name = $pos->name;
                $positions[$key]->old_price = $pos->price;
                $true_price += $positions[$key]->true_price;
                $this->positions[$key] = $positions[$key];
            }
            $this->total_price = $price;
            $this->total_true_price = $true_price;
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'orders';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Orders[]|Orders|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Orders|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
