<?php

class Positions extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Column(column="order_id", type="integer", length=10, nullable=false)
     */
    public $order_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="type_id", type="integer", length=10, nullable=false)
     */
    public $type_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(column="type_name", type="string", length=50, nullable=false)
     */
    public $type_name;

    /**
     *
     * @var double
     * @Column(column="price", type="double", nullable=false)
     */
    public $price;

    /**
     *
     * @var integer
     * @Column(column="count", type="integer", length=10, nullable=false)
     */
    public $count;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("positions");
        $this->belongsTo('order_id', '\Orders', 'order_id', ['alias' => 'Orders']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'positions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Positions[]|Positions|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Positions|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
