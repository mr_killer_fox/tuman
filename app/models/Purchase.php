<?php

class Purchase extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="purchase_id", type="integer", length=10, nullable=false)
     */
    public $purchase_id;

    /**
     *
     * @var integer
     * @Column(column="product_id", type="integer", length=11, nullable=true)
     */
    public $product_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=255)
     */
    public $name;

    /**
     *
     * @var double
     * @Column(column="price", type="double")
     */
    public $price;

    /**
     *
     * @var integer
     * @Column(column="count", type="integer", length=10, nullable=false)
     */
    public $count;

    /**
     *
     * @var string
     * @Column(column="date_add", type="string", nullable=false)
     */
    public $date_add;

    /**
     *
     * @var string
     * @Column(column="is_buy", type="int", length=1)
     */
    public $is_buy;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("purchase");
        $this->belongsTo('product_id', '\Products', 'product_id', ['alias' => 'Products']);
    }

    public function afterFetch()
    {
        if ($this->product_id) {
            $product = Products::findFirst($this->product_id);
            $this->name = $product->name;
            $this->price = $product->price;
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'purchase';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Purchase[]|Purchase|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Purchase|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
