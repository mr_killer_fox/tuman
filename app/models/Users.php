<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Users extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=10, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $email;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $password;

    /**
     *
     * @var tinyint
     * @Column(type="tinyint", nullable=false)
     */
    public $active;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        // $this->setSchema("kl_test9");
        $this->setSource("users");
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users[]|Users|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null, $active = true)
    {
        return parent::find(self::getParams($parameters, $active));
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null, $active = true)
    {
        return parent::findFirst(self::getParams($parameters, $active));
    }

    private static function getParams($parameters = null, $active = true) {
        if ($active) {
            if (is_array($parameters)) {
                if (array_key_exists('conditions', $parameters)) {
                    $parameters['conditions'] .= ' AND active = 1';
                } else {
                    $parameters['conditions'] = 'active = 1';
                }
            } else {
                if (isset($parameters)) {
                    if (strpos($parameters, '=') !== false) {
                        $parameters .= ' AND active = 1';
                    } else {
                        $parameters = 'user_id = '.$parameters.' AND active = 1';
                    }
                } else {
                    $parameters = 'active = 1';
                }
            }
            return $parameters;
        } else {
            return $parameters;
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }

}
