{% extends 'layouts/page.volt' %}

{% block meta %}
    <title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../cocktails/save", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <select name="product_id[]" class="js-select2 js_select_product" data-style="form-control" data-menu-style="" multiple="multiple" required="" >
                        <option value="0" disabled>Выберете продукт</option>
                        {% for index,product in products %}
                            <option value="{{product.product_id}}"
                                {% for cp in cps %}
                                    {% if cp.product_id == product.product_id %}
                                        selected
                                    {% endif %}
                                {% endfor %}
                            >{{product.name}}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-4 js_litter_insert">
                    {% for cp in cps %}
                        <div class="form-group">
                            <label for="fieldLitter{{ cp.product_id }}">{{ cp.name }}</label>
                            <input type="text" id="fieldLitter{{ cp.product_id }}" name="litter[{{ cp.product_id }}]" data-id="{{ cp.product_id }}" class="form-control" value="{{ cp.litter }}" placeholder="Литраж" required>
                        </div>
                    {% endfor %}
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ text_field("name","required":true,"class":'form-control',"placeholder":'Название', "id" : "fieldName") }}
                    </div>
                    <div class="form-group">
                        {{ text_field("price","required":true,"class":'form-control',"placeholder":'Цена', "id" : "fieldPrice") }}
                        {{ hidden_field('cocktail_id','value':cocktail_id) }}
                    </div>
                </div>
                <div class="col-md-12">
                    {{ submit_button("Обновить","class":'btn btn-success btn-simple btn-round') }}
                    {{ link_to("/../cocktails", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}

{% block js %}
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function(){
            $('.js_select_product').change(function() {
                $(this).find('option').each(function() {
                    if ($('.js_litter_insert').find('[data-id='+$(this).val()+']').length == 0 && $(this).is(':selected')) {
                        $('.js_litter_insert').append('<div class="form-group"><label for="fieldLitter'+$(this).val()+'">'+$(this).text()+'</label><input type="text" id="fieldLitter'+$(this).val()+'" name="litter['+$(this).val()+']" data-id="'+$(this).val()+'" class="form-control" placeholder="Литраж" required></div>');
                    } else if (!$(this).is(':selected') && $('.js_litter_insert').find('[data-id='+$(this).val()+']').length > 0) {
                        console.log(1);
                        $('.js_litter_insert').find('[data-id='+$(this).val()+']').closest('.form-group').remove();
                    }
                });
            });
        });
    </script>
{% endblock %}