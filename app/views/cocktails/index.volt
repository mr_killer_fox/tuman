{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
            	{{ link_to("/../cocktails/new","Добавить","class":'btn btn-primary') }}
            	{% if not(page.items is empty) %}
            		<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th class="text-left">Название</th>
				            <th class="text-left">Литраж</th>
				            <th class="text-left">Цена закупки</th>
				            <th class="text-left">Цена продажи</th>
				            <th></th>
				        </tr>
            			{% for cocktail in page.items %}
            				{% if cocktail.CocktailProduct|length > 1 %}
						        <tr class="bg-primary">
			                        <td class="text-left">
			                           	{{ cocktail.cocktail_id }}
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.name }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.total_litter }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.total_price }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.price }}	
			                        </td>
			                        <td class="text-right">
			                             <div class="btn-group" role="group">
			                                 <a href="/../cocktails/edit/{{cocktail.cocktail_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
			                                 <a href="/../cocktails/delete/{{cocktail.cocktail_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
			                             </div>
			                        </td>
			                    </tr>
			                    {% for cp in cocktail.products %}
				                    <tr>
				                        <td class="text-left">
				                           	{{ cp.product_id }}
				                        </td>
				                        <td class="text-left">
				                            {{ cp.name }}	
				                        </td>
				                        <td class="text-left">
				                            {{ cp.litter }}	
				                        </td>
				                      	<td class="text-left">
				                            {{ cp.price }}	
				                        </td>
				                      	<td></td>
				                      	<td></td>
				                    </tr>
				                {% endfor %}
				                <tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				            {% endif %}
		           		{% endfor %}
		           		{% for cocktail in page.items %}
            				{% if cocktail.CocktailProduct|length == 1 %}
						        <tr class="bg-primary">
			                        <td class="text-left">
			                           	{{ cocktail.cocktail_id }}
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.name }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.total_litter }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.total_price }}	
			                        </td>
			                        <td class="text-left">
			                            {{ cocktail.price }}	
			                        </td>
			                        <td class="text-right">
			                             <div class="btn-group" role="group">
			                                 <a href="/../cocktails/edit/{{cocktail.cocktail_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
			                                 <a href="/../cocktails/delete/{{cocktail.cocktail_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
			                             </div>
			                        </td>
			                    </tr>
			                    {% for cp in cocktail.products %}
				                    <tr>
				                        <td class="text-left">
				                           	{{ cp.product_id }}
				                        </td>
				                        <td class="text-left">
				                            {{ cp.name }}	
				                        </td>
				                        <td class="text-left">
				                            {{ cp.litter }}	
				                        </td>
				                      	<td class="text-left">
				                            {{ cp.price }}	
				                        </td>
				                      	<td></td>
				                      	<td></td>
				                    </tr>
				                {% endfor %}
				                <tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				            {% endif %}
		           		{% endfor %}
		            </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../cocktails?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету коктелей
	            {% endif %}   
	            {{ link_to("/../cocktails/new","Добавить","class":'btn btn-primary') }}
            </div>
        </div>
    </div>
{% endblock %}