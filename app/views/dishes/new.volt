{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../dishes/create", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {{ text_field("name","required":true,"class":'form-control',"placeholder":'Название', "id" : "fieldName") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ text_field("price","required":true,"class":'form-control',"placeholder":'Цена', "id" : "fieldPrice") }}
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Добавить","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../dishes", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}