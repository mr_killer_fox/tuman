{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}
{% block body %}    
    <body class="login-page">
{% endblock %} 
{% block header %}
    <nav class="navbar navbar-toggleable-md bg-primary fixed-top navbar-transparent " color-on-scroll="500">
        <div class="container">
            <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-header">Main menu</a>
                    <a class="dropdown-item" href="/">Return to home page</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Contact page</a>
                </div>
            </div>
            <div class="navbar-translate">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="now-ui-icons files_paper"></i>
                            <p>Back</p>
                        </a>
                    </li>
                </ul>
            </div>        
        </div>
    </nav>
{% endblock %}
{% block wrapper %}  
	<div class="page-header" filter-color="orange">
{% endblock %}
{% block main %}
{% endblock %}
{% block content %}
    <div class="page-header-image"></div>
    <div class="container">
        <div class="col-md-4 content-center">
	    	{{ flash.output() }}
            <div class="card card-login card-plain">
                {{ form("/../index/signup") }}
                    <div class="header header-primary text-center">
                        <div class="logo-container">
                            <img src="{{ this.url.get('/../img/klookva-logo-white.png') }}" alt="">
                        </div>
                    </div>
                    <div class="content">
					   	<div class="input-group form-group-no-border input-lg">
                            <span class="input-group-addon">
                                <i class="now-ui-icons users_circle-08"></i>
                            </span>
					        {{ email_field("email","required":true,"class":'form-control',"placeholder":'Email...') }}
					    </div>
                    	<div class="input-group form-group-no-border input-lg">
					        <span class="input-group-addon">
                                <i class="now-ui-icons ui-1_lock-circle-open"></i>
                            </span>
					        {{ password_field("password","required":true,"class":'form-control',"placeholder":'Password...') }}
					    </div>
					    <div class="footer text-center">
					    	{{ hidden_field("signup","class":'form-control',"value":'1') }}
					    	{{ submit_button("Log in","class":'btn btn-primary btn-round btn-lg btn-block') }}
                        </div>
                        <div class="pull-left">
                            <h6>
                                <a href="#pablo" class="link">Request account</a>
                            </h6>
                        </div>
					</div>
				{{ end_form() }}
        	</div>
    	</div>
    </div>
{% endblock %}
{% block endmain %} 
{% endblock %}