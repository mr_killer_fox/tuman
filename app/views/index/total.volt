{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
    	<input type="hidden" class="js_url_for_ajax" value="/{{ need_id }}">
    	<input type="hidden" class="js_start_date" value="{{ start_date }}">
	    <h2 class="title">{{ title }}</h2>
	    <div class="row">
			<div class="col-md-12">
	            <div class="datepicker-container">
	                <div class="form-group">
	                    <input type="text" class="form-control date-picker" value="{{ datepick }}" data-datepicker-color="primary">
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="row js_main_dasboard">
	        <div class="col-md-8">
	            <table class='table'>
	                <tr>
	                    <td class="text-left">Итог потрачено</td>
	                    <td>{{ total_buy }} грн.</td>
	                </tr>
	                <tr>
	                    <td class="text-left">Итог продано</td>
	                    <td>{{ total_sell }} грн.</td>
	                </tr>
	            </table>
	        </div>
	        <div class="col-md-4">
	            <div class="card" data-background-color="orange">
	                <div class="card-block">
	                    <div class="title">Выпито алкоголя</div>
	                    <table class="table table-condensed">
	                    	{% for product in products %}
		                        <tr>
		                            <td class="text-left">{{ product['name'] }}</td>
		                            <td class="text-right">{{ roundMe(product['price'], 2) }}грн. за {{ product['litter'] / 1000 }}л.</td>
		                        </tr>
		                    {% endfor %}
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<script type="text/javascript" charset="utf-8">
        document.addEventListener("DOMContentLoaded", function () {
        	$(".date-picker").datepicker( {
			    format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months"
			});
            $('.date-picker').focus(function (){
                $.ajax({
                    url: '/../index/total',
                    type: 'POST',
                    data: 'date='+$(this).val(),
                    success: function(data){
                        $('.js_main_dasboard').html($($(data)).find('.js_main_dasboard').html());
                    },
                });
            });
        });
    </script>
{% endblock %}