<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1">  
    	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		{% block meta %}
        	<!-- META -->
        {% endblock %}
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    	<!--     Fonts and icons     -->
	    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	    <!-- CSS Files -->
	    <link href="{{ this.url.get('/../css/bootstrap2.min.css') }}" rel="stylesheet" />
	    <link href="{{ this.url.get('/../css/now-ui-kit.css') }}" rel="stylesheet" />
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

	    <link rel="shortcut icon" type="image/x-icon" href="{{ this.url.get('/../img/klookva_logo_ico_32.png') }}"/>
	    <link rel="stylesheet" href="{{ this.url.get('/../css/custom.css') }}">
        {% block css %}
	        <!-- CSS -->
    	{% endblock %}
    </head>
{% block body %}    
    <body class="index-page">
{% endblock %}    	
    	{% block header %}
	        <nav class="navbar navbar-toggleable-md bg-default fixed-top" -color-on-scroll="500">
		        <div class="container">
		            <div class="navbar-translate">
		                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
		                    <span class="navbar-toggler-bar bar1"></span>
		                    <span class="navbar-toggler-bar bar2"></span>
		                    <span class="navbar-toggler-bar bar3"></span>
		                </button>
		                <a class="navbar-brand" href="/" rel="tooltip" title="Hello and welcome to kLooKva admin dashboard" data-placement="bottom" target="_blank">
		                    Туман Бар
		                </a>
		            </div>
		            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
		                <ul class="navbar-nav">
		                	<li class="nav-item dropdown">
		                        <a class="nav-link dropdown-toggle" href="/../orders" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                            <i class="now-ui-icons shopping_cart-simple"></i>
		                            <p>Заказы</p>
		                        </a>
		                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		                            <a class="dropdown-header">Заказы</a>
		                            <a class="dropdown-item" href="/../orders">Открытые</a>
		                            <a class="dropdown-item" href="/../orders/closed">Закрытые</a>
		                        </div>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" href="/../purchase">
		                            <i class="now-ui-icons business_money-coins"></i>
		                            <p>Закупка</p>
		                        </a>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" href="/../cocktails">
		                            <i class="now-ui-icons business_bulb-63"></i>
		                            <p>Коктели</p>
		                        </a>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" href="/../products">
		                            <i class="now-ui-icons business_chart-bar-32"></i>
		                            <p>Ингредиенты</p>
		                        </a>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" href="/../index/total">
		                            <i class="now-ui-icons business_chart-bar-32"></i>
		                            <p>Итоги</p>
		                        </a>
		                    </li>
		                    <li class="nav-item dropdown">
		                        <a class="nav-link dropdown-toggle" href="login.html" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                            <i class="now-ui-icons ui-1_settings-gear-63"></i>
		                            <p>Системное</p>
		                        </a>
		                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		                            <a class="dropdown-header">Пользователи</a>
		                            <a class="dropdown-item" href="/../users">Пользователи</a>
		                            <a class="dropdown-item" href="/../role">Роли</a>
		                            <div class="dropdown-divider"></div>
		                            <a class="dropdown-header">Хлам</a>
		                            <a class="dropdown-item" href="/../tabels">Столы</a>
		                            <a class="dropdown-item" href="/../dishes">Посуда</a>
		                        </div>
		                    </li>
		                    <li class="nav-item">
		                        <a class="nav-link" href="/../index/logout">
		                            <i style="vertical-align: middle" class="now-ui-icons sport_user-run"></i>
		                            <p>Выход</p>
		                        </a>
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </nav>
    	{% endblock %}
    	{% block wrapper %} 
	    	<div class="wrapper">
	    {% endblock %}
	    {%block afterwrapper %}
	    {% endblock %}
	    {% block main %}
    		<div class="main">
        		<div class="section section-basic">
        {% endblock %}
	    	{% block messages %}
	    		{{ flash.output() }}
    		{% endblock %}
	    	{% block content %}
	        {% endblock %}
	        {% block endmain %}
    				</div>
        		</div>
        	{% endblock %}
        	{% block beforefooter %}
        	{% endblock %}
	        <footer  class="footer" data-background-color="black">
				{% block footer %}
					<div class="container">
		                <nav>
		                    <ul>
		                        <li>
		                            <a href="http://klookva.com.ua">
		                                klookva homepage
		                            </a>
		                        </li>
		                    </ul>
		                </nav>
		                <div class="copyright">
		                    &copy;
		                    <script>
		                        document.write(new Date().getFullYear());
		                    </script>, Coded by kLooKva
		                </div>
            		</div>
				{% endblock %}
			</footer>
	    </div>
		<script src="{{ this.url.get('/../js/core/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
		<script src="{{ this.url.get('/../js/core/tether.min.js') }}" type="text/javascript"></script>
		<script src="{{ this.url.get('/../js/core/bootstrap.min.js') }}" type="text/javascript"></script>
		<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
		<script src="{{ this.url.get('/../js/plugins/bootstrap-switch.js') }}"></script>
		<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
		<script src="{{ this.url.get('/../js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
		<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
		<script src="{{ this.url.get('/../js/plugins/bootstrap-datepicker.js') }}" type="text/javascript"></script>
		<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
		<script src="{{ this.url.get('/../js/now-ui-kit.js') }}" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		<script type="text/javascript">
		    $(document).ready(function() {
		        
		        /*nowuiKit.initSliders();*/
		        $('.js-select2').select2();
		    });

		    function scrollToDownload() {
		        if ($('.section-download').length != 0) {
		            $("html, body").animate({
		                scrollTop: $('.section-download').offset().top
		            }, 1000);
		        }
		    }
		</script>
	    <script src="{{ this.url.get('/../js/custom.js') }}"></script>
		{% block js %}
		 	<!-- JS -->
	    {% endblock %}
	</body>
</html>
