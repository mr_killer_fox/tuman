{% extends 'layouts/page.volt' %}

{% block meta %}
    <title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <div class="datepicker-container">
                            <div class="form-group">
                                <input type="text" class="form-control date-picker" value="{{ datepick }}" data-datepicker-color="primary">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <input type="text" class="form-control js_total_price_closed" value="{{ total_price_closed }}" data-datepicker-color="primary" disabled>
                        </div>
                    </div>
                </div>
                <div class="js_orders">
                    {% if not(page.items is empty) %}
                        <table class="table">
                            <tr>
                                <th class="text-left">Ид</th>
                                <th class="text-left">Ид стола</th>
                                <th class="text-left">Бармен</th>
                                <th class="text-left">Позиции</th>
                                <th class="text-left">Цена со скидкой</th>
                                <th class="text-left">Цена без скидки</th>
                                <th class="text-left">Дата открытия</th>
                                <th class="text-left">Дата обновления</th>
                                <th></th>
                            </tr>
                            {% for order in page.items %}
                                <tr>
                                    <td class="text-left">
                                        {{ order.order_id }}
                                    </td>
                                    <td class="text-left">
                                        {{ order.tabel_id }}    
                                    </td>
                                    <td class="text-left">
                                        {{ order.Users.name }}  
                                    </td>
                                    <td class="text-left">
                                        {% for position in order.positions %}
                                            {{ position.name }} <b>{{ position.count }}</b>шт. по <b>{{ position.price }}</b>грн.<br>
                                        {% endfor %}
                                    </td>
                                    <td class="text-left">
                                        {{ order.total_price }} 
                                    </td>
                                    <td class="text-left">
                                        {{ order.total_true_price }}    
                                    </td>
                                    <td class="text-left">
                                        {{ order.date_add }}    
                                    </td>
                                    <td class="text-left">
                                        {{ order.date_upd }}    
                                    </td>
                                    <td class="text-right">
                                         <div class="btn-group" role="group">
                                            <a href="/../orders/delete/{{order.order_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
                                         </div>
                                    </td>
                                </tr>
                            {% endfor %}
                        </table>
                        <ul class="pagination pagination-primary">
                            {% set currentPage = request.get('page') ? request.get('page') : 1 %}
                            {% for i in 1..page.total_pages %}
                                <li class="page-item {% if (i == currentPage) %}active{% endif %}">
                                    <a class="page-link" href="/../orders/closed?page={{i}}">{{i}}</a>
                                </li>
                            {% endfor %} 
                        </ul>
                    {% else %}
                        Нету закрытых заказов
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" charset="utf-8">
        document.addEventListener("DOMContentLoaded", function () {
            $('.date-picker').focus(function (){
                $.ajax({
                    url: '/../orders/closed',
                    type: 'POST',
                    data: 'date='+$(this).val(),
                    success: function(data){
                        $('.js_orders').html($($(data)).find('.js_orders').html());
                        $('.js_total_price_closed').val($($(data)).find('.js_total_price_closed').val());
                    },
                });
            });
        });
    </script>
{% endblock %}