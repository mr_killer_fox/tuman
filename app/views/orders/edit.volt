{% extends 'layouts/page.volt' %}

{% block meta %}
    <title>{{ title }}</title>
{% endblock %}

{% block css %}
<style type="text/css" media="screen">
    .list-wrapper {
      overflow: hidden;
    }

    #sortable-listA, #sortable-listB, #sortable-listC, #sortable-listD, .sortable-hint {
      width: 45%;
      min-height: 40px;
      margin: 0;
      padding: 0;
      border: 1px solid #dddddd;
      border-radius: 4px;
    }

    #sortable-listA, #sortable-listC {
      float: left;
    }

    #sortable-listB, #sortable-listD {
      float: right;
    }

    .list-item {
      list-style-type: none;
      width: calc(100% - 10px);
      margin: 5px;
      line-height: 30px;
      text-align: center;
      background-color: #222222;
      color: #ffffff;
      border-radius: 3px;
      cursor: move;
    }

    #sortable-listA .list-item {
      background-color: #54b8fa;
      color: #000000;
    }

    #sortable-listB .list-item {
      background-color: #ff879e;
      color: #000000;
    }

    .sortable-hint .list-item {
      background-color: #54b8fa;
      color: #000000;
    }

    #placeholder.list-item {
      background-color: #ffffff;
      color: #777;
    }

    .state-selected {
      background-color: #ff0 !important;
    }

    @media screen and (max-width: 1023px) {
      .demo-section {
        display: none;
      }
    }
</style>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../orders/save", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-12">
                    {{ submit_button("Изменить","class":'btn btn-success btn-simple btn-round') }}
                    <div data-href="/../orders/closeorder/{{ order_id }}" class="btn btn-warning btn-simple btn-round js_closed_order">Закрыть</div>
                    {{ link_to("/../orders", "Назад") }}
                </div>
            </div>
            <br>
            {{ hidden_field('order_id','value':order_id) }}
            <div class="row">
               <div class="col-md-3">
                    <label>Номер стола</label><br>
                    <select name="tabel_id" class="js-select2" data-style="form-control" data-menu-style="" required="" >
                        <option value="0" disabled>Выберете стол</option>
                        {% for index,tabel in tabels %}
                            <option value="{{tabel.tabel_id}}" {% if tabel.tabel_id == tabel_id %}selected{% endif %}>{{tabel.tabel_id}}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Стоимость всего</label>
                        {{ text_field("price","disabled":true,"class":'form-control text-danger') }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Стоимость покупки</label>
                        {{ text_field("total_price","disabled":true,"class":'form-control text-danger') }}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Коментарий</label>
                        {{ text_area("comment","class":'form-control') }}
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <div class="list-wrapper">
                        <ul id="sortable-listC">
                            <li class="list-item state-selected" data-value="cocktails">
                               Коктели
                            </li>
                            <li class="list-item" data-value="cocktail_singles">
                               Продукты
                            </li>
                            <li class="list-item" data-value="dishes">
                               Посуда
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 js_items_order">
                    <label class="js_name_cocktails js_names">Коктели</label>
                    <label class="js_name_cocktail_singles js_names" style="display:none;">Продукты</label>
                    <label class="js_name_dishes js_names" style="display:none;">Посуда</label>
                    <div class="list-wrapper">
                        <ul id="sortable-listA">
                            {% for cocktail in cocktails %}
                                {% if cocktail.CocktailProduct|length > 1 %}
                                    {% set flag = 0 %}
                                    {% for position in positions %}
                                        {% if position.type_name == 'cocktail' and position.type_id == cocktail.cocktail_id %}
                                            {% set flag = 1 %}
                                        {% endif %}
                                    {% endfor %}
                                    {% if flag == 0 %}
                                        <li class="list-item js_cocktail_item" data-value="{{ cocktail.cocktail_id }}" data-price="{{ cocktail.price }}">
                                            {{ cocktail.name }}
                                        </li>
                                    {% endif %}
                                {% endif %}
                            {% endfor %}
                            {% for cocktail in cocktails %}
                                {% if cocktail.CocktailProduct|length == 1 %}
                                    {% set flag = 0 %}
                                    {% for position in positions %}
                                        {% if position.type_name == 'cocktail_single' and position.type_id == cocktail.cocktail_id %}
                                            {% set flag = 1 %}
                                        {% endif %}
                                    {% endfor %}
                                    {% if flag == 0 %}
                                        <li class="list-item js_cocktail_single_item" data-value="{{ cocktail.cocktail_id }}" data-price="{{ cocktail.price }}" style="display:none;">
                                            {{ cocktail.name }}
                                        </li>
                                    {% endif %}    
                                {% endif %}
                            {% endfor %}
                            {% for dishe in dishes %}
                                {% set flag = 0 %}
                                {% for position in positions %}
                                    {% if position.type_name == 'dishe' and position.type_id == dishe.dishe_id %}
                                        {% set flag = 1 %}
                                    {% endif %}
                                {% endfor %}
                                {% if flag == 0 %}
                                    <li class="list-item js_dishe_item" data-value="{{ dishe.dishe_id }}" data-price="{{ dishe.price }}" style="display:none;">
                                        {{ dishe.name }}
                                    </li>
                                {% endif %} 
                            {% endfor %}
                        </ul>
                        <ul id="sortable-listB">
                            {% for position in positions %}
                                <li class="list-item js_{{ position.type_name }}_item" data-value="{{ position.type_id }}" data-price="{{ position.old_price }}">
                                    {{ position.name }}
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 js_prices">
                    {% for position in positions %}
                        <div class="form-group">
                            <label for="fieldPrice{{ position.type_name }}-{{ position.type_id }}">
                                Цена для {{ position.name }}
                            </label>
                            <input type="text" id="fieldPrice{{ position.type_name }}-{{ position.type_id }}" name="price_{{ position.type_name }}[{{ position.type_id }}]" data-id="{{ position.type_name }}-{{ position.type_id }}" class="form-control" data-item_purchase="{{ position.type_name }}" data-id_purchase="{{ position.type_id }}" placeholder="Цена" onkeydown="keydown_count(event)" onblur="countSellPrice()" value="{{ position.price }}" required>
                        </div>
                    {% endfor %}
                </div>
                <div class="col-md-3 js_counters">
                    {% for position in positions %}
                        <div class="form-group">
                            <label for="fieldCount{{ position.type_name }}-{{ position.type_id }}">
                                Количество для {{ position.name }}
                            </label>
                            <br>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round js_dec" type="button" style="display: inline-block; float: left;" onclick="add_dec(this)">
                                -
                            </button>
                            <input type="text" style="display: inline-block;width: 70%;" id="fieldCount{{ position.type_name }}-{{ position.type_id }}" name="count_{{ position.type_name }}[{{ position.type_id }}]" data-id="{{ position.type_name }}-{{ position.type_id }}" class="form-control js_counter_one" data-item_purchase="{{ position.type_name }}" data-id_purchase="{{ position.type_id }}" placeholder="Количество" value="{{ position.count }}" required onkeydown="keydown_count(event)" onblur="countSellPrice()">
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round js_inc" type="button" style="display: inline-block; float: right;"onclick="add_inc(this)">
                                +
                            </button>
                        </div>
                    {% endfor %}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    {{ submit_button("Изменить","class":'btn btn-success btn-simple btn-round') }}
                    <div data-href="/../orders/closeorder/{{ order_id }}" class="btn btn-warning btn-simple btn-round js_closed_order">Закрыть</div>
                    {{ link_to("/../orders", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}

{% block js %}
    <script src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script>
        if ($('#sortable-listA').height() >= $('#sortable-listB').height()) {
            $('#sortable-listB').height($('#sortable-listA').height());
        } else {
            $('#sortable-listB').height('100%');
        }
        function placeholder(element) {
          return $("<li class='list-item' id='placeholder'>Drop Here!</li>");
        }

        var listA = $("#sortable-listA").kendoSortable({
          connectWith: "#sortable-listB",
          placeholder: placeholder,
          hint: function(element) {
            var hint = $("<div class='sortable-hint'></div>");
            $("#sortable-listA").find(".state-selected").clone().css("display", "block").appendTo(hint);

            setTimeout(function() {
              hint.children().show();
            });

            return hint;
          },
          start: function(e) {
            $("#sortable-listA").find(".state-selected").hide();
          },
          end: function(e) {
            var items = this.element.find(".state-selected").not(e.item);
            items.insertAfter(this.placeholder).show();
          },
          change: function(e) {
            if ($('#sortable-listA').find('.list-item:visible').length >= $('#sortable-listB').find('.list-item').length) {
                $('#sortable-listB').height($('#sortable-listA').height());
            } else {
                $('#sortable-listB').height('100%');
            }

            $('#sortable-listB').find('.list-item').each(function() {
                var prefix = '';
                if ($(this).hasClass('js_cocktail_item')) {
                    prefix = 'cocktail';
                } else if ($(this).hasClass('js_cocktail_single_item')) {
                    prefix = 'cocktail_single';
                } else if ($(this).hasClass('js_dishe_item')) {
                    prefix = 'dishe';
                }
                if ($('.js_prices').find('[data-id='+prefix+'-'+$(this).data('value')+']').length == 0) {
                    $('.js_prices').append('<div class="form-group"><label for="fieldPrice'+prefix+'-'+$(this).data('value')+'">Цена для '+$(this).text()+'</label><input type="text" id="fieldPrice'+prefix+'-'+$(this).data('value')+'" name="price_'+prefix+'['+$(this).data('value')+']" data-id="'+prefix+'-'+$(this).data('value')+'" class="form-control" data-item_purchase="'+prefix+'" data-id_purchase="'+$(this).data('value')+'" placeholder="Цена" onkeydown="keydown_count(event)" onblur="countSellPrice()" value="'+$(this).data('price')+'" required></div>');
                }
                if ($('.js_counters').find('[data-id='+prefix+'-'+$(this).data('value')+']').length == 0) {
                    $('.js_counters').append('<div class="form-group"><label for="fieldCount'+prefix+'-'+$(this).data('value')+'">Количество для '+$(this).text()+'</label><br><button class="btn btn-primary btn-icon btn-icon-mini btn-round js_dec" type="button" style="display: inline-block; float: left;" onclick="add_dec(this)">-</button><input type="text" style="display: inline-block;width: 70%;" id="fieldCount'+prefix+'-'+$(this).data('value')+'" name="count_'+prefix+'['+$(this).data('value')+']" data-id="'+prefix+'-'+$(this).data('value')+'" class="form-control js_counter_one" data-item_purchase="'+prefix+'" data-id_purchase="'+$(this).data('value')+'" placeholder="Количество" value="1" required onkeydown="keydown_count(event)" onblur="countSellPrice()"><button class="btn btn-primary btn-icon btn-icon-mini btn-round js_inc" type="button" style="display: inline-block; float: right;"onclick="add_inc(this)">+</button></div>');
                }
            });

            $('.js_prices').find('input').each(function(){
                if ($('#sortable-listB').find('.list-item.js_'+$(this).data('item_purchase')+'_item[data-value='+$(this).data('id_purchase')+']').length == 0) {
                    $(this).closest('.form-group').remove();
                }
            });

            $('.js_counters').find('input').each(function(){
                if ($('#sortable-listB').find('.list-item.js_'+$(this).data('item_purchase')+'_item[data-value='+$(this).data('id_purchase')+']').length == 0) {
                    $(this).closest('.form-group').remove();
                }
            });

            countSellPrice();

            $("#sortable-listA").find(".state-selected").removeClass("state-selected");
          }
        }).data("kendoSortable");

        listA.draggable.userEvents.bind("tap", function(e) {
          if (e.event.ctrlKey) {
            e.target.toggleClass("state-selected")  
          } else {
            $(".state-selected").removeClass("state-selected");
            e.target.addClass("state-selected");
          }
        });
        $("#sortable-listB").kendoSortable({
          placeholder: placeholder,
          connectWith: "#sortable-listA"
        });

        var listC = $("#sortable-listC").kendoSortable({
          placeholder: placeholder,
          hint: function(element) {
            var hint = $("<div class='sortable-hint'></div>");
            $("#sortable-listC").find(".state-selected").clone().css("display", "block").appendTo(hint);

            setTimeout(function() {
              hint.children().show();
            });

            return hint;
          },
          start: function(e) {
            $("#sortable-listC").find(".state-selected").hide();
          },
          end: function(e) {
            var items = this.element.find(".state-selected").not(e.item);
            items.insertAfter(this.placeholder).show();
          }
        }).data("kendoSortable");

        listC.draggable.userEvents.bind("tap", function(e) {
            $(".state-selected").removeClass("state-selected");
            e.target.addClass("state-selected");
            $('#sortable-listA > .list-item').hide();
            $('#sortable-listA').closest('.js_items_order').find('.js_names').hide();
            if (e.target.filter('[data-value=cocktails]').length > 0){
                $('#sortable-listA > .js_cocktail_item').show();
                $('#sortable-listA').closest('.js_items_order').find('.js_name_cocktails').show();
            } else if (e.target.filter('[data-value=cocktail_singles]').length > 0) {
                $('#sortable-listA > .js_cocktail_single_item').show();
                $('#sortable-listA').closest('.js_items_order').find('.js_name_cocktail_singles').show();
            } else if (e.target.filter('[data-value=dishes]').length > 0) {
                $('#sortable-listA > .js_dishe_item').show();
                $('#sortable-listA').closest('.js_items_order').find('.js_name_dishes').show();
            }
            if ($('#sortable-listA').height() >= $('#sortable-listB').height()) {
                $('#sortable-listB').height($('#sortable-listA').height());
            } else {
                $('#sortable-listB').height('100%');
            }
        });
    </script>
    <script type="text/javascript" charset="utf-8">
        function keydown_count(e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
         function add_inc(t) {
            if ($(t).parent().find('input').val() == '') {
                $(t).parent().find('input').val(1);
            } else {
                $(t).parent().find('input').val(parseInt($(t).parent().find('input').val())+1);
            }
            countSellPrice();
        }
        function add_dec(t) {
            if ($(t).parent().find('input').val() == '' || $(t).parent().find('input').val() == 1) {
                $(t).parent().find('input').val(1);
            } else {
                $(t).parent().find('input').val(parseInt($(t).parent().find('input').val())-1);
            }
            countSellPrice();
        }

        function countSellPrice () {
            var sellprice = 0;
            var totalprice = 0;
            $('.js_prices').find('input').each(function() {
                sellprice += (parseFloat($(this).val()) * parseInt($('.js_counters').find('input[data-id_purchase='+$(this).data('id_purchase')+'][data-item_purchase='+$(this).data('item_purchase')+']').val()));
                totalprice += (parseFloat($('#sortable-listB').find('.list-item.js_'+$(this).data('item_purchase')+'_item[data-value='+$(this).data('id_purchase')+']').data('price')) * parseInt($('.js_counters').find('input[data-id_purchase='+$(this).data('id_purchase')+'][data-item_purchase='+$(this).data('item_purchase')+']').val()));
            });
            $('#total_price').val(sellprice);
            $('#price').val(totalprice);
        }
    </script>
    <script type="text/javascript" charset="utf-8">
        $('.js_closed_order').click(function(){
            $(this).closest('form').attr('action', $(this).data('href'));
            $(this).closest('form').find('input[type=submit]').click();
        });
    </script>
{% endblock %}