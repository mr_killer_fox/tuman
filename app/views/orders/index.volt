{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
            	{{ link_to("/../orders/new","Добавить","class":'btn btn-primary') }}
            	{% if not(page.items is empty) %}
            		<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th class="text-left">Ид стола</th>
				            <th class="text-left">Бармен</th>
				            <th class="text-left">Позиции</th>
				            <th class="text-left">Цена со скидкой</th>
				            <th class="text-left">Цена без скидки</th>
				            <th class="text-left">Дата открытия</th>
				            <th class="text-left">Дата обновления</th>
				            <th></th>
				        </tr>
            			{% for order in page.items %}
					        <tr>
		                        <td class="text-left">
		                           	{{ order.order_id }}
		                        </td>
		                        <td class="text-left">
		                            {{ order.tabel_id }}	
		                        </td>
		                        <td class="text-left">
		                            {{ order.Users.name }}	
		                        </td>
		                        <td class="text-left">
		                        	{% for position in order.positions %}
		                           		{{ position.name }} <b>{{ position.count }}</b>шт. по <b>{{ position.price }}</b>грн.<br>
		                            {% endfor %}
		                        </td>
		                        <td class="text-left">
		                            {{ order.total_price }}	
		                        </td>
		                        <td class="text-left">
		                            {{ order.total_true_price }}	
		                        </td>
		                        <td class="text-left">
		                            {{ order.date_add }}	
		                        </td>
		                        <td class="text-left">
		                            {{ order.date_upd }}	
		                        </td>
		                        <td class="text-right">
		                             <div class="btn-group" role="group">
		                                 <a href="/../orders/edit/{{order.order_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
		                                 <a href="/../orders/closeorder/{{order.order_id}}" class="btn btn-danger btn-sm"><small>закрыть</small></a>
		                             </div>
		                        </td>
		                    </tr>
		            	{% endfor %}
		            </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../orders?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету активных заказов
	            {% endif %}
            </div>
        </div>
    </div>
{% endblock %}