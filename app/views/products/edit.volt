{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../products/save", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("name","required":true,"class":'form-control',"placeholder":'Название', "id" : "fieldName") }}
                    	{{ hidden_field('product_id','value':product_id) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("litter","class":'form-control',"placeholder":'Литраж', "id" : "fieldLitter") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ text_field("price","required":true,"class":'form-control',"placeholder":'Цена', "id" : "fieldPrice") }}
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Обновить","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../products", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}