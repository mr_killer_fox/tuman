{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
            	{% if not(page.items is empty) %}
					<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th class="text-left">Название</th>
				            <th class="text-left">Литраж</th>
				            <th class="text-left">Цена</th>
				            <th></th>
				        </tr>
				        {% for product in page.items %}
					        <tr>
		                        <td class="text-left">
		                           	{{ product.product_id }}
		                        </td>
		                        <td class="text-left">
		                            {{ product.name }}	
		                        </td>
		                        <td class="text-left">
		                            {{ product.litter }}	
		                        </td>
		                        <td class="text-left">
		                            {{ product.price }}	
		                        </td>
		                        <td class="text-right">
		                             <div class="btn-group" role="group">
		                                 <a href="/../products/edit/{{product.product_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
		                                 <a href="/../products/delete/{{product.product_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
		                             </div>
		                        </td>
		                    </tr>
	                    {% endfor %}
	                </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../products?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету товаров
	            {% endif %}   
	            {{ link_to("/../products/new","Добавить","class":'btn btn-primary') }}
            </div>
        </div>
    </div>
{% endblock %}