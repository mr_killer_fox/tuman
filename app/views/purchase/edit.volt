{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../purchase/save", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <select name="product_id" class="js-select2" data-style="form-control" data-menu-style="">
                            <option value="0">Выберете продукт</option>
                            {% for index,product in products %}
                                <option value="{{product.product_id}}" 
                                    {% if product.product_id == product_id %}selected{% endif %}
                                >{{product.name}}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div class="form-group">
                    	{{ text_field("name","class":'form-control',"placeholder":'Или введите название продукта', "id" : "fieldName") }}
                    	{{ hidden_field('purchase_id','value':purchase_id) }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("count","required":true,"class":'form-control',"placeholder":'Количество', "id" : "fieldCount") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {{ text_field("price","class":'form-control',"placeholder":'Цена', "id" : "fieldPrice") }}
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <input id="fieldIsBuy" type="checkbox" name="is_buy" {% if is_buy == 1 %}checked{% endif %}>
                            <label for="fieldIsBuy"> Куплено </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Обновить","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../purchase", "Back") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}

{% block js %}
    <script type="text/javascript">
        $(document).ready(function() {
            if ($('[name=product_id]').val() != 0) {
                $('#fieldName').prop('disabled', true);
                $('#fieldPrice').prop('disabled', true);
            } else {
                $('#fieldName').prop('disabled', false);
                $('#fieldPrice').prop('disabled', false);
            }
            $('[name=product_id]').change(function(){
                if ($('[name=product_id]').val() != 0) {
                    $('#fieldName').prop('disabled', true);
                    $('#fieldPrice').prop('disabled', true);
                } else {
                    $('#fieldName').prop('disabled', false);
                    $('#fieldPrice').prop('disabled', false);
                }
            });
        });
    </script>
{% endblock %}