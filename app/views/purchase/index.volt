{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
			<div class="col-md-12">
	            <div class="datepicker-container">
	                <div class="form-group">
	                    <input type="text" class="form-control date-picker" value="{{ datepick }}" data-datepicker-color="primary">
	                </div>
	            </div>
	        </div>
	    </div>
        <div class="row js_main_purchase">
            <div class="col-md-12">
            	{% if not(page.items is empty) %}
					<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th class="text-left">Название</th>
				            <th class="text-left">Цена</th>
				            <th class="text-left">Количество</th>
				            <th class="text-left">Дата</th>
				            <th class="text-left">Куплено</th>
				            <th></th>
				        </tr>
				        {% for purchase in page.items %}
					        <tr>
		                        <td class="text-left">
		                           	{{ purchase.purchase_id }}
		                        </td>
		                        <td class="text-left">
		                            {{ purchase.name }}	
		                        </td>
		                        <td class="text-left">
		                            {{ purchase.price }}	
		                        </td>
		                        <td class="text-left">
		                            {{ purchase.count }}	
		                        </td>
		                        <td class="text-left">
		                            {{ purchase.date_add }}	
		                        </td>
		                        <td class="text-left">
		                            {% if purchase.is_buy == 1 %}
					            		<a href="/../purchase/buy/{{purchase.purchase_id}}/0/{{ request.get('page') ? request.get('page') : 1 }}"><i class="fa fa-check"></i></a>
						            {% else %}
						            	<a href="/../purchase/buy/{{purchase.purchase_id}}/1/{{ request.get('page') ? request.get('page') : 1 }}"><i class="fa fa-times"></i></a>
						            {% endif %}	
		                        </td>
		                        <td class="text-right">
		                             <div class="btn-group" role="group">
		                                 <a href="/../purchase/edit/{{purchase.purchase_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
		                                 <a href="/../purchase/delete/{{purchase.purchase_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
		                             </div>
		                        </td>
		                    </tr>
	                    {% endfor %}
	                </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../purchase?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету закупок
	            {% endif %}   
	            {{ link_to("/../purchase/new","Добавить","class":'btn btn-primary') }}
            </div>
        </div>
    </div>
    <script type="text/javascript" charset="utf-8">
        document.addEventListener("DOMContentLoaded", function () {
        	$(".date-picker").datepicker( {
			    format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months"
			});
            $('.date-picker').focus(function (){
                $.ajax({
                    url: '/../purchase',
                    type: 'POST',
                    data: 'date='+$(this).val(),
                    success: function(data){
                        $('.js_main_purchase').html($($(data)).find('.js_main_purchase').html());
                    },
                });
            });
        });
    </script>
{% endblock %}