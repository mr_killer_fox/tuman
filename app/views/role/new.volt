{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../role/create", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("name","required":true,"class":'form-control',"placeholder":'Name', "id" : "fieldName") }}
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Add","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../role", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}