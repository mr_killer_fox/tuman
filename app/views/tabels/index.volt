{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
            	{% if not(page.items is empty) %}
					<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th></th>
				        </tr>
				        {% for tabel in page.items %}
					        <tr>
		                        <td class="text-left">
		                           	{{ tabel.tabel_id }}
		                        </td>
		                        <td class="text-right">
		                             <div class="btn-group" role="group">
		                                 <a href="/../tabels/delete/{{tabel.tabel_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
		                             </div>
		                        </td>
		                    </tr>
	                    {% endfor %}
	                </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../tabels?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету столов
	            {% endif %}   
	            {{ link_to("/../tabels/create","Добавить","class":'btn btn-primary') }}
            </div>
        </div>
    </div>
{% endblock %}