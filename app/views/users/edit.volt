{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../users/save", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("name","required":true,"class":'form-control',"placeholder":'Имя', "id" : "fieldName") }}
                    	{{ hidden_field('user_id','value':user_id) }}
                    </div>
                     <div class="form-group">
                    	{{ email_field("email","required":true,"class":'form-control',"placeholder":'Email', "id" : "fieldEmail") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ password_field("password","class":'form-control',"placeholder":'Пароль', "id" : "fieldPassword") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select name="user_role[]" class="selectpicker form-control" data-style="form-control" data-menu-style="" multiple>
                            <option disabled>User role</option>
                            {% for index,role in roles %}
                            	<option value="{{role.role_id}}" 
                            	{% for selector in select_role %}
                            		{% if role.role_id == selector %}selected{% endif %}
                            	{% endfor %}
                            	>{{role.name}}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="checkbox">
                            <input id="fieldActive" type="checkbox" name="active" {% if user_active == 1 %}checked{% endif %}>
                            <label for="fieldActive"> Active </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Обновить","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../users", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}