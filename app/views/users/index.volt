{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
    <div class="container">
        <h3 class="title">{{title}}</h3>
        <div class="row">
            <div class="col-md-12">
            	{% if not(page.items is empty) %}
					<table class="table">
				        <tr>
				            <th class="text-left">Ид</th>
				            <th class="text-left">Название</th>
				            <th class="text-left">Email</th>
				            <th class="text-left">Активный</th>
				            <th></th>
				        </tr>
				        {% for user in page.items %}
					        <tr>
		                        <td class="text-left">
		                           	{{ user.user_id }}
		                        </td>
		                        <td class="text-left">
		                            {{ user.name }}	
		                        </td>
		                        <td class="text-left">
		                            {{ user.email }}	
		                        </td>
		                        <td class="text-left">
		                            {% if user.active == 1 %}
					            		<i class="fa fa-check"></i>
						            {% else %}
						            	<i class="fa fa-times"></i>
						            {% endif %}	
		                        </td>
		                        <td class="text-right">
		                             <div class="btn-group" role="group">
		                                 <a href="/../users/edit/{{user.user_id}}" class="btn btn-success btn-sm"><small>редактировать</small></a>
		                                 <a href="/../users/delete/{{user.user_id}}" class="btn btn-danger btn-sm"><small>удалить</small></a>
		                             </div>
		                        </td>
		                    </tr>
	                    {% endfor %}
	                </table>
	                <ul class="pagination pagination-primary">
	                	{% set currentPage = request.get('page') ? request.get('page') : 1 %}
	                	{% for i in 1..page.total_pages %}
		                    <li class="page-item {% if (i == currentPage) %}active{% endif %}">
		                        <a class="page-link" href="/../users?page={{i}}">{{i}}</a>
		                    </li>
		                {% endfor %} 
	                </ul>
	            {% else %}
	            	Нету пользователей
	            {% endif %}   
	            {{ link_to("/../users/new","Добавить","class":'btn btn-primary') }}
            </div>
        </div>
    </div>
{% endblock %}