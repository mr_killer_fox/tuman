{% extends 'layouts/page.volt' %}

{% block meta %}
	<title>{{ title }}</title>
{% endblock %}

{% block content %}
	<div class="container">
        <h3 class="title">{{ h1 }}</h3>
         {{ form("/../users/create", "autocomplete" : "off") }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ text_field("name","required":true,"class":'form-control',"placeholder":'Name', "id" : "fieldName") }}
                    </div>
                     <div class="form-group">
                    	{{ email_field("email","required":true,"class":'form-control',"placeholder":'Email', "id" : "fieldEmail") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    	{{ password_field("password","required":true,"class":'form-control',"placeholder":'Password', "id" : "fieldName") }}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select name="user_role[]" class="selectpicker form-control" data-style="form-control" data-menu-style="" multiple>
                            <option disabled>User role</option>
                            {% for role in roles %}
                            	<option value="{{role.role_id}}" {% if loop.first %}selected{% endif %}>{{role.name}}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <div class="checkbox">
                            <input id="fieldActive" type="checkbox" name="active">
                            <label for="fieldActive"> Active </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                	{{ submit_button("Add","class":'btn btn-success btn-simple btn-round') }}
	        		{{ link_to("/../users", "Назад") }}
                </div>
            </div>
        {{ end_form() }}
    </div>
{% endblock %}