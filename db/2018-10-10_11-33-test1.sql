-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Жов 10 2018 р., 11:32
-- Версія сервера: 5.7.23-0ubuntu0.16.04.1
-- Версія PHP: 7.2.5-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `test1`
--

-- --------------------------------------------------------

--
-- Структура таблиці `cocktails`
--

CREATE TABLE `cocktails` (
  `cocktail_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `cocktails`
--

INSERT INTO `cocktails` (`cocktail_id`, `name`, `price`) VALUES
(1, 'Мохито алкогольный', 70);

-- --------------------------------------------------------

--
-- Структура таблиці `cocktail_product`
--

CREATE TABLE `cocktail_product` (
  `cocktail_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `litter` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `cocktail_product`
--

INSERT INTO `cocktail_product` (`cocktail_id`, `product_id`, `litter`) VALUES
(1, 8, 50),
(1, 11, 80),
(1, 12, 3),
(1, 25, 100);

-- --------------------------------------------------------

--
-- Структура таблиці `dishes`
--

CREATE TABLE `dishes` (
  `dishe_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `dishes`
--

INSERT INTO `dishes` (`dishe_id`, `name`, `price`) VALUES
(1, 'Стакан 200', 50);

-- --------------------------------------------------------

--
-- Структура таблиці `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) NOT NULL,
  `tabel_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_upd` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` text,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `orders`
--

INSERT INTO `orders` (`order_id`, `tabel_id`, `user_id`, `date_add`, `date_upd`, `comment`, `active`) VALUES
(1, 1, 1, '2018-09-30 17:23:15', '2018-10-09 23:38:26', NULL, 0),
(2, 1, 1, '2018-10-09 23:46:24', '2018-10-09 23:46:24', '', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `positions`
--

CREATE TABLE `positions` (
  `order_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `type_name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `positions`
--

INSERT INTO `positions` (`order_id`, `type_id`, `type_name`, `price`, `count`) VALUES
(1, 1, 'cocktail', 70, 2),
(1, 2, 'product', 50, 1),
(1, 3, 'product', 550, 1),
(2, 1, 'cocktail', 70, 1),
(2, 2, 'product', 600, 2),
(2, 4, 'product', 50, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `litter` int(10) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`product_id`, `name`, `litter`, `price`) VALUES
(1, 'Водка absolut ', 3000, 250),
(2, 'Виски Jack deniels', 1000, 600),
(3, 'Виски Jameson', 1000, 550),
(4, 'Виски Hankey bannister', 1000, 320),
(5, 'Абсент', 1000, 300),
(6, 'Текила Montezuma', 1000, 300),
(7, 'Ликер jagermeister', 1000, 450),
(8, 'Ром белый caribica', 1000, 250),
(9, 'Джин', 1000, 350),
(10, 'Трипл сек', 1000, 250),
(11, 'Лайм', 1000, 213),
(12, 'Мята', 1000, 290),
(13, 'Томатный сок', 1930, 32.2),
(14, 'Лимонный сок', 950, 24.9),
(15, 'Соус Tobasco', 60, 130),
(16, 'Ликер Malibu(кокос)', 500, 144),
(17, 'Гренадин', 700, 120),
(18, 'Апельсиновый сок', 950, 23.9),
(19, 'Ром черный Bacardi', 1000, 400),
(20, 'Ликер Baileys', 700, 450),
(21, 'Самбука Toso', 700, 230),
(22, 'Ликер Блю кюрасао', 700, 255),
(23, 'Ликер pisang', 700, 180),
(24, 'Кола', 18000, 166.4),
(25, 'Спрайт', 18000, 166.4),
(26, 'Сок манго', 950, 22.9),
(27, 'Персиковый сок', 1930, 33.8),
(28, 'Водка smirnoff', 3000, 250),
(29, 'Текила Montezuma Gold', 1000, 300),
(30, 'Коньяк Курвуазье', 1000, 670),
(31, 'Коньяк Атикус', 1000, 225),
(32, 'Шампанское Fragolino', 700, 80),
(33, 'Шампанское Мартини', 700, 225),
(34, 'Тоник', 6000, 134.9),
(35, 'Энергетик Ред булл', 4800, 129.9),
(36, 'Морс клюква', 950, 24.9),
(37, 'водка finlandia', 1000, 359.9),
(38, 'персиковый ликор peatchtree', 700, 349),
(39, 'Marengo special ', 700, 74.9),
(40, 'Сироп личи', 500, 65),
(41, 'Сироп зелёное яблоко', 500, 65),
(42, 'Сироп манго ', 500, 65),
(43, 'настойка персик', 500, 83),
(44, 'лимон', 1000, 75),
(45, 'швепс', 1000, 18.9),
(46, 'Кофейный ликёр', 700, 245);

-- --------------------------------------------------------

--
-- Структура таблиці `purchase`
--

CREATE TABLE `purchase` (
  `purchase_id` int(10) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `count` int(10) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_buy` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `product_id`, `name`, `price`, `count`, `date_add`, `is_buy`) VALUES
(1, 1, 'Водка absolut ', 250, 1, '2018-09-18 04:24:20', 1),
(2, 2, 'Виски Jack deniels', 600, 1, '2018-09-18 04:24:52', 1),
(3, 3, 'Виски Jameson', 550, 1, '2018-09-18 04:24:58', 1),
(4, 4, 'Виски Hankey bannister', 320, 1, '2018-09-18 04:25:04', 0),
(5, 5, 'Абсент', 300, 1, '2018-09-18 04:25:11', 0),
(6, 6, 'Текила Montezuma', 300, 1, '2018-09-18 04:25:20', 0),
(7, 7, 'Ликер jagermeister', 450, 1, '2018-09-18 04:25:29', 1),
(8, 8, 'Ром белый caribica', 250, 1, '2018-09-18 04:25:42', 0),
(9, 9, 'Джин', 350, 1, '2018-09-18 04:25:52', 0),
(10, 10, 'Трипл сек', 250, 1, '2018-09-18 04:26:02', 0),
(11, 11, 'Лайм', 213, 1, '2018-09-18 04:26:13', 0),
(12, 12, 'Мята', 290, 1, '2018-09-18 04:26:24', 0),
(13, 13, 'Томатный сок', 32.2, 1, '2018-09-18 04:26:32', 0),
(14, 14, 'Лимонный сок', 24.9, 1, '2018-09-18 04:26:43', 0),
(15, 15, 'Соус Tobasco', 130, 1, '2018-09-18 04:26:50', 0),
(16, 16, 'Ликер Malibu(кокос)', 144, 1, '2018-09-18 04:27:01', 0),
(17, 17, 'Гренадин', 120, 1, '2018-09-18 04:27:13', 0),
(18, 18, 'Апельсиновый сок', 23.9, 1, '2018-09-18 04:27:23', 0),
(19, 19, 'Ром черный Bacardi', 400, 1, '2018-09-18 04:27:34', 0),
(20, 20, 'Ликер Baileys', 450, 1, '2018-09-18 04:27:42', 0),
(21, 21, 'Самбука Toso', 230, 1, '2018-09-18 04:27:51', 0),
(22, 22, 'Ликер Блю кюрасао', 255, 1, '2018-09-18 04:28:09', 0),
(23, 23, 'Ликер pisang', 180, 1, '2018-09-18 04:28:17', 0),
(24, 24, 'Кола', 166.4, 1, '2018-09-18 04:28:27', 0),
(25, 25, 'Спрайт', 166.4, 1, '2018-09-18 04:28:36', 0),
(26, 26, 'Сок манго', 22.9, 1, '2018-09-18 04:28:44', 0),
(27, 27, 'Персиковый сок', 33.8, 1, '2018-09-18 04:28:53', 0),
(28, 28, 'Водка smirnoff', 250, 1, '2018-09-18 04:29:03', 0),
(29, 29, 'Текила Montezuma Gold', 300, 1, '2018-09-18 04:29:47', 0),
(30, 30, 'Коньяк Курвуазье', 670, 1, '2018-09-18 04:29:55', 0),
(31, 31, 'Коньяк Атикус', 225, 1, '2018-09-18 04:30:05', 0),
(32, 32, 'Шампанское Fragolino', 80, 1, '2018-09-18 04:30:14', 0),
(33, 33, 'Шампанское Мартини', 225, 1, '2018-09-18 04:30:27', 0),
(34, 34, 'Тоник', 134.9, 1, '2018-09-18 04:30:35', 0),
(35, 35, 'Энергетик Ред булл', 129.9, 1, '2018-09-18 04:30:45', 0),
(36, 36, 'Морс клюква', 24.9, 1, '2018-09-18 04:30:59', 0),
(37, 37, 'водка finlandia', 359.9, 1, '2018-09-18 04:31:20', 0),
(38, 38, 'персиковый ликор peatchtree', 349, 1, '2018-09-18 04:31:33', 0),
(39, 39, 'Marengo special ', 74.9, 1, '2018-09-18 04:31:44', 0),
(40, 40, 'Сироп личи', 65, 1, '2018-09-18 04:31:53', 0),
(41, 41, 'Сироп зелёное яблоко', 65, 1, '2018-09-18 04:32:03', 0),
(42, 42, 'Сироп манго ', 65, 1, '2018-09-18 04:32:12', 0),
(43, 43, 'настойка персик', 83, 1, '2018-09-18 04:32:22', 0),
(44, 44, 'лимон', 75, 1, '2018-09-18 04:32:29', 0),
(45, 45, 'швепс', 18.9, 1, '2018-09-18 04:32:37', 0),
(46, 46, 'Кофейный ликёр', 245, 1, '2018-09-18 04:32:45', 0),
(47, NULL, 'Аренда', 3000, 1, '2018-09-18 04:33:40', 0),
(48, NULL, 'Шейкер европейский', 158, 1, '2018-09-18 04:37:35', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `role`
--

CREATE TABLE `role` (
  `role_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `role`
--

INSERT INTO `role` (`role_id`, `name`) VALUES
(1, 'Admin'),
(2, 'Employee'),
(3, 'Guest');

-- --------------------------------------------------------

--
-- Структура таблиці `tabels`
--

CREATE TABLE `tabels` (
  `tabel_id` int(10) NOT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `tabels`
--

INSERT INTO `tabels` (`tabel_id`, `is_locked`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `active`) VALUES
(1, 'Admin', 'admin@klookva.com.ua', '$2y$08$Z05FK3lyUEZCYVB3RVJwduGrZurPofect2Atj8lhk7iCdtUfRPY.m', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(1, 1);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `cocktails`
--
ALTER TABLE `cocktails`
  ADD PRIMARY KEY (`cocktail_id`);

--
-- Індекси таблиці `cocktail_product`
--
ALTER TABLE `cocktail_product`
  ADD PRIMARY KEY (`cocktail_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `dishes`
--
ALTER TABLE `dishes`
  ADD PRIMARY KEY (`dishe_id`);

--
-- Індекси таблиці `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `tabel_id` (`tabel_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`order_id`,`type_id`,`type_name`),
  ADD KEY `order_id` (`order_id`);

--
-- Індекси таблиці `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Індекси таблиці `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Індекси таблиці `tabels`
--
ALTER TABLE `tabels`
  ADD PRIMARY KEY (`tabel_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Індекси таблиці `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `cocktails`
--
ALTER TABLE `cocktails`
  MODIFY `cocktail_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `dishes`
--
ALTER TABLE `dishes`
  MODIFY `dishe_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT для таблиці `purchase`
--
ALTER TABLE `purchase`
  MODIFY `purchase_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблиці `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `tabels`
--
ALTER TABLE `tabels`
  MODIFY `tabel_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `cocktail_product`
--
ALTER TABLE `cocktail_product`
  ADD CONSTRAINT `cocktail_product_ibfk_1` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`cocktail_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cocktail_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`tabel_id`) REFERENCES `tabels` (`tabel_id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Обмеження зовнішнього ключа таблиці `positions`
--
ALTER TABLE `positions`
  ADD CONSTRAINT `positions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
