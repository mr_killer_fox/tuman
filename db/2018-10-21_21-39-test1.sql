-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Час створення: Жов 21 2018 р., 21:39
-- Версія сервера: 10.2.11-MariaDB
-- Версія PHP: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `admin_tuman`
--

-- --------------------------------------------------------

--
-- Структура таблиці `cocktails`
--

CREATE TABLE `cocktails` (
  `cocktail_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `cocktails`
--

INSERT INTO `cocktails` (`cocktail_id`, `name`, `price`) VALUES
(1, 'Мохито алкогольный', 70),
(2, 'Лонг Айленд', 85),
(3, 'Кровавая мери', 40),
(4, 'Секс на пляже', 55),
(5, 'Текила санрайз', 45),
(6, 'Виски кола', 55),
(7, 'КУба Либре ', 55),
(8, 'Голубая лагуна', 50),
(9, 'Водка энергетик', 45),
(10, 'Абсент с энергетиком', 50),
(11, 'Выстрел в голову', 150),
(12, 'Мохито б/а', 50),
(13, 'джин тоник', 50),
(14, 'Ягербомба', 65),
(15, 'Хиросима', 55),
(16, 'Зеленый мексеканец', 50),
(17, 'Стивен Сигал', 55),
(18, 'Б-52', 55),
(19, 'Б-53', 55),
(20, 'Манго', 35),
(21, 'Пилот', 35),
(23, 'Бабл Гам', 30),
(24, 'Красный Пёс', 60),
(25, 'Голубые Гавайи', 85),
(26, 'Большой Лебовски', 70),
(27, 'Marengo special', 50),
(28, 'Абсент', 35),
(29, 'Виски Hankey bannister', 40),
(30, 'Виски Jack deniels', 50),
(31, 'Виски Jameson', 50),
(32, 'Водка absolut', 25),
(33, 'водка finlandia', 35),
(34, 'Водка smirnoff', 25),
(35, 'Джин', 35),
(36, 'Coca-Cola 0,5', 25),
(37, 'Коньяк Атикус', 30),
(38, 'Коньяк Курвуазье', 65),
(39, 'Пиво Corona', 50),
(40, 'Ликер Baileys', 50),
(41, 'Ликер jagermeister', 50),
(42, 'Ром белый caribica', 35),
(43, 'Ром черный Bacardi', 50),
(44, 'Самбука Toso', 35),
(45, 'Текила Montezuma', 40),
(46, 'Текила Montezuma Gold', 40),
(47, 'Тоник 0,5', 30),
(48, 'Sprite 0,5', 25),
(49, 'Шампанское Fragolino', 150),
(50, 'Шампанское Мартини', 350),
(51, 'Энергетик Ред булл', 40),
(52, 'Энергетик burn', 35);

-- --------------------------------------------------------

--
-- Структура таблиці `cocktail_product`
--

CREATE TABLE `cocktail_product` (
  `cocktail_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `litter` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `cocktail_product`
--

INSERT INTO `cocktail_product` (`cocktail_id`, `product_id`, `litter`) VALUES
(1, 8, 50),
(1, 11, 80),
(1, 12, 3),
(1, 25, 100),
(2, 1, 30),
(2, 6, 30),
(2, 8, 30),
(2, 9, 30),
(2, 10, 30),
(2, 14, 30),
(2, 24, 100),
(3, 1, 50),
(3, 13, 120),
(3, 14, 10),
(3, 15, 1),
(4, 1, 50),
(4, 18, 40),
(4, 36, 40),
(4, 38, 25),
(5, 6, 50),
(5, 17, 10),
(5, 18, 40),
(6, 2, 50),
(6, 24, 150),
(7, 14, 20),
(7, 19, 50),
(7, 24, 120),
(8, 1, 50),
(8, 22, 20),
(8, 25, 150),
(9, 1, 50),
(9, 35, 150),
(9, 44, 40),
(10, 5, 50),
(10, 35, 150),
(11, 2, 50),
(11, 5, 50),
(11, 6, 50),
(11, 21, 50),
(11, 45, 200),
(12, 11, 80),
(12, 12, 3),
(12, 25, 150),
(13, 9, 50),
(13, 34, 100),
(14, 7, 50),
(14, 35, 150),
(15, 5, 15),
(15, 17, 5),
(15, 20, 10),
(15, 21, 20),
(16, 6, 10),
(16, 14, 10),
(16, 23, 20),
(17, 15, 5),
(17, 17, 20),
(17, 21, 25),
(18, 10, 15),
(18, 20, 15),
(18, 46, 15),
(19, 5, 15),
(19, 20, 15),
(19, 46, 15),
(20, 14, 10),
(20, 19, 20),
(20, 42, 10),
(20, 43, 10),
(21, 5, 25),
(21, 35, 25),
(23, 1, 20),
(23, 18, 15),
(23, 41, 10),
(24, 6, 20),
(24, 15, 5),
(24, 21, 25),
(25, 8, 50),
(25, 16, 20),
(25, 22, 50),
(25, 47, 100),
(26, 1, 50),
(26, 16, 20),
(26, 46, 50),
(26, 48, 50),
(27, 39, 200),
(28, 5, 50),
(29, 4, 50),
(30, 2, 50),
(31, 3, 50),
(32, 1, 50),
(33, 37, 50),
(34, 28, 50),
(35, 9, 50),
(36, 50, 500),
(37, 31, 50),
(38, 30, 50),
(39, 11, 10),
(39, 49, 330),
(40, 20, 50),
(41, 7, 50),
(42, 8, 50),
(43, 19, 50),
(44, 21, 50),
(45, 6, 50),
(46, 29, 50),
(47, 51, 500),
(48, 52, 500),
(49, 32, 700),
(50, 33, 700),
(51, 54, 250),
(52, 53, 250);

-- --------------------------------------------------------

--
-- Структура таблиці `dishes`
--

CREATE TABLE `dishes` (
  `dishe_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `dishes`
--

INSERT INTO `dishes` (`dishe_id`, `name`, `price`) VALUES
(1, 'Стакан 200', 30),
(2, 'Стакан 420', 60),
(3, 'Стопка 50', 15),
(4, 'Бокал винный', 60);

-- --------------------------------------------------------

--
-- Структура таблиці `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) NOT NULL,
  `tabel_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT current_timestamp(),
  `date_upd` datetime NOT NULL DEFAULT current_timestamp(),
  `comment` text DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `orders`
--

INSERT INTO `orders` (`order_id`, `tabel_id`, `user_id`, `date_add`, `date_upd`, `comment`, `active`) VALUES
(1, 1, 2, '2018-10-20 01:46:57', '2018-10-20 01:46:57', '', 99),
(2, 1, 2, '2018-10-20 01:50:39', '2018-10-20 01:50:39', '', 0),
(3, 1, 1, '2018-10-20 23:02:06', '2018-10-21 01:48:06', '', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `positions`
--

CREATE TABLE `positions` (
  `order_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `type_name` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `positions`
--

INSERT INTO `positions` (`order_id`, `type_id`, `type_name`, `price`, `count`) VALUES
(2, 6, 'cocktail', 55, 1),
(3, 6, 'cocktail', 55, 1),
(3, 13, 'cocktail', 50, 3),
(3, 36, 'cocktail_single', 25, 1),
(3, 39, 'cocktail', 50, 2),
(3, 43, 'cocktail_single', 50, 5);

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `litter` int(10) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`product_id`, `name`, `litter`, `price`) VALUES
(1, 'Водка absolut ', 3000, 250),
(2, 'Виски Jack deniels', 1000, 600),
(3, 'Виски Jameson', 1000, 550),
(4, 'Виски Hankey bannister', 1000, 320),
(5, 'Абсент', 1000, 300),
(6, 'Текила Montezuma', 1000, 300),
(7, 'Ликер jagermeister', 1000, 450),
(8, 'Ром белый caribica', 1000, 250),
(9, 'Джин', 1000, 350),
(10, 'Трипл сек', 1000, 250),
(11, 'Лайм', 1000, 213),
(12, 'Мята', 1000, 290),
(13, 'Томатный сок', 1930, 32.2),
(14, 'Лимонный сок', 950, 24.9),
(15, 'Соус Tobasco', 60, 130),
(16, 'Ликер Malibu(кокос)', 500, 144),
(17, 'Гренадин', 700, 120),
(18, 'Апельсиновый сок', 950, 23.9),
(19, 'Ром черный Bacardi', 1000, 400),
(20, 'Ликер Baileys', 700, 450),
(21, 'Самбука Toso', 700, 230),
(22, 'Ликер Блю кюрасао', 700, 255),
(23, 'Ликер pisang', 700, 180),
(24, 'Кола', 18000, 166.4),
(25, 'Спрайт', 18000, 166.4),
(26, 'Сок манго', 950, 22.9),
(27, 'Персиковый сок', 1930, 33.8),
(28, 'Водка smirnoff', 3000, 250),
(29, 'Текила Montezuma Gold', 1000, 300),
(30, 'Коньяк Курвуазье', 1000, 670),
(31, 'Коньяк Атикус', 1000, 225),
(32, 'Шампанское Fragolino', 700, 80),
(33, 'Шампанское Мартини', 700, 225),
(34, 'Тоник', 6000, 134.9),
(35, 'Энергетик Ред булл', 4800, 129.9),
(36, 'Морс клюква', 950, 24.9),
(37, 'водка finlandia', 1000, 359.9),
(38, 'персиковый ликор peatchtree', 700, 349),
(39, 'Marengo special ', 700, 74.9),
(40, 'Сироп личи', 500, 65),
(41, 'Сироп зелёное яблоко', 500, 65),
(42, 'Сироп манго ', 500, 65),
(43, 'настойка персик', 500, 83),
(44, 'лимон', 1000, 75),
(45, 'швепс', 1000, 18.9),
(46, 'Кофейный ликёр', 700, 245),
(47, 'Ананасовый сок', 950, 19.44),
(48, 'Сливки', 200, 14.9),
(49, 'Пиво Corona', 330, 24.4),
(50, 'Coca-Cola 0,5', 500, 9.4),
(51, 'Тоник 0,5', 500, 15),
(52, 'Sprite 0,5', 500, 9.4),
(53, 'Энергетик burn', 250, 18.3),
(54, 'Энергетик red bull 0,25', 250, 21.7);

-- --------------------------------------------------------

--
-- Структура таблиці `purchase`
--

CREATE TABLE `purchase` (
  `purchase_id` int(10) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `count` int(10) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT current_timestamp(),
  `is_buy` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `product_id`, `name`, `price`, `count`, `date_add`, `is_buy`) VALUES
(1, 1, 'Водка absolut ', 250, 1, '2018-10-21 01:48:06', 1),
(2, 2, 'Виски Jack deniels', 600, 1, '2018-10-21 01:48:06', 1),
(3, 3, 'Виски Jameson', 550, 1, '2018-10-21 01:48:06', 1),
(4, 4, 'Виски Hankey bannister', 320, 1, '2018-10-21 01:48:06', 1),
(5, 5, 'Абсент', 300, 1, '2018-10-21 01:48:06', 1),
(6, 6, 'Текила Montezuma', 300, 1, '2018-10-21 01:48:06', 1),
(7, 7, 'Ликер jagermeister', 450, 1, '2018-10-21 01:48:06', 1),
(8, 8, 'Ром белый caribica', 250, 1, '2018-10-21 01:48:06', 1),
(9, 9, 'Джин', 350, 1, '2018-10-21 01:48:06', 0),
(10, 10, 'Трипл сек', 250, 1, '2018-10-21 01:48:06', 0),
(11, 11, 'Лайм', 213, 1, '2018-10-21 01:48:06', 0),
(12, 12, 'Мята', 290, 1, '2018-10-21 01:48:06', 0),
(13, 13, 'Томатный сок', 32.2, 1, '2018-10-21 01:48:06', 0),
(14, 14, 'Лимонный сок', 24.9, 1, '2018-10-21 01:48:06', 0),
(15, 15, 'Соус Tobasco', 130, 1, '2018-10-21 01:48:06', 0),
(16, 16, 'Ликер Malibu(кокос)', 144, 1, '2018-10-21 01:48:06', 0),
(17, 17, 'Гренадин', 120, 1, '2018-10-21 01:48:06', 0),
(18, 18, 'Апельсиновый сок', 23.9, 1, '2018-10-21 01:48:06', 0),
(19, 19, 'Ром черный Bacardi', 400, 1, '2018-10-21 01:48:06', 0),
(20, 20, 'Ликер Baileys', 450, 1, '2018-10-21 01:48:06', 0),
(21, 21, 'Самбука Toso', 230, 1, '2018-10-21 01:48:06', 0),
(22, 22, 'Ликер Блю кюрасао', 255, 1, '2018-10-21 01:48:06', 0),
(23, 23, 'Ликер pisang', 180, 1, '2018-10-21 01:48:06', 0),
(24, 24, 'Кола', 166.4, 1, '2018-10-21 01:48:06', 0),
(25, 25, 'Спрайт', 166.4, 1, '2018-10-21 01:48:06', 0),
(26, 26, 'Сок манго', 22.9, 1, '2018-10-21 01:48:06', 0),
(27, 27, 'Персиковый сок', 33.8, 1, '2018-10-21 01:48:06', 0),
(28, 28, 'Водка smirnoff', 250, 1, '2018-10-21 01:48:06', 0),
(29, 29, 'Текила Montezuma Gold', 300, 1, '2018-10-21 01:48:06', 0),
(30, 30, 'Коньяк Курвуазье', 670, 1, '2018-10-21 01:48:06', 0),
(31, 31, 'Коньяк Атикус', 225, 1, '2018-10-21 01:48:06', 0),
(32, 32, 'Шампанское Fragolino', 80, 1, '2018-10-21 01:48:06', 0),
(33, 33, 'Шампанское Мартини', 225, 1, '2018-10-21 01:48:06', 0),
(34, 34, 'Тоник', 134.9, 1, '2018-10-21 01:48:06', 0),
(35, 35, 'Энергетик Ред булл', 129.9, 1, '2018-10-21 01:48:06', 0),
(36, 36, 'Морс клюква', 24.9, 1, '2018-10-21 01:48:06', 0),
(37, 37, 'водка finlandia', 359.9, 1, '2018-10-21 01:48:06', 0),
(38, 38, 'персиковый ликор peatchtree', 349, 1, '2018-10-21 01:48:06', 0),
(39, 39, 'Marengo special ', 74.9, 1, '2018-10-21 01:48:06', 0),
(40, 40, 'Сироп личи', 65, 1, '2018-10-21 01:48:06', 0),
(41, 41, 'Сироп зелёное яблоко', 65, 1, '2018-10-21 01:48:06', 0),
(42, 42, 'Сироп манго ', 65, 1, '2018-10-21 01:48:06', 0),
(43, 43, 'настойка персик', 83, 1, '2018-10-21 01:48:06', 0),
(44, 44, 'лимон', 75, 1, '2018-10-21 01:48:06', 0),
(45, 45, 'швепс', 18.9, 1, '2018-10-21 01:48:06', 0),
(46, 46, 'Кофейный ликёр', 245, 1, '2018-10-21 01:48:06', 0),
(47, NULL, 'Аренда', 3000, 1, '2018-10-21 01:48:06', 0),
(48, NULL, 'Шейкер европейский', 158, 1, '2018-10-21 01:48:06', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `role`
--

CREATE TABLE `role` (
  `role_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `role`
--

INSERT INTO `role` (`role_id`, `name`) VALUES
(1, 'Admin'),
(2, 'Employee'),
(3, 'Guest');

-- --------------------------------------------------------

--
-- Структура таблиці `tabels`
--

CREATE TABLE `tabels` (
  `tabel_id` int(10) NOT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `tabels`
--

INSERT INTO `tabels` (`tabel_id`, `is_locked`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `active`) VALUES
(1, 'Антиков Е.', 'ea@klookva.com.ua', '$2y$08$V251Rmh1eEdNL0Y5amE1M.RK5OAaGiRZVGSc9hsphW5nMO5d0M4MC', 1),
(2, 'Музыка А.', 'gameplay26@gmail.com', '$2y$08$M0FEdzduRGg0d0h0dS9kW.TONG6GwPpvcCwLwQhBVEt/P6DEf189a', 1),
(3, 'Кисель Е.', 'zazaby212@gmail.com', '$2y$08$YUtZT285ZUVuTXI0b0c3U.HqZEFUNDIWSzZDDcHvJ6VzYIS8xtgbe', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 2);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `cocktails`
--
ALTER TABLE `cocktails`
  ADD PRIMARY KEY (`cocktail_id`);

--
-- Індекси таблиці `cocktail_product`
--
ALTER TABLE `cocktail_product`
  ADD PRIMARY KEY (`cocktail_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `dishes`
--
ALTER TABLE `dishes`
  ADD PRIMARY KEY (`dishe_id`);

--
-- Індекси таблиці `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `tabel_id` (`tabel_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Індекси таблиці `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`order_id`,`type_id`,`type_name`),
  ADD KEY `order_id` (`order_id`);

--
-- Індекси таблиці `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Індекси таблиці `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Індекси таблиці `tabels`
--
ALTER TABLE `tabels`
  ADD PRIMARY KEY (`tabel_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Індекси таблиці `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `cocktails`
--
ALTER TABLE `cocktails`
  MODIFY `cocktail_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT для таблиці `dishes`
--
ALTER TABLE `dishes`
  MODIFY `dishe_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT для таблиці `purchase`
--
ALTER TABLE `purchase`
  MODIFY `purchase_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблиці `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `tabels`
--
ALTER TABLE `tabels`
  MODIFY `tabel_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `cocktail_product`
--
ALTER TABLE `cocktail_product`
  ADD CONSTRAINT `cocktail_product_ibfk_1` FOREIGN KEY (`cocktail_id`) REFERENCES `cocktails` (`cocktail_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cocktail_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`tabel_id`) REFERENCES `tabels` (`tabel_id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Обмеження зовнішнього ключа таблиці `positions`
--
ALTER TABLE `positions`
  ADD CONSTRAINT `positions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
