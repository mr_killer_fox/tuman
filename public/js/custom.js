$('.js_filter').on('change', function () {
  	var url = $(this).val();
  	if (url) {
      	window.location = url;
  	}
  return false;
});
$(document).ready(function(){
	if (!$('#on_off_from').prop('checked')) {
		$('#fieldMin_d').prop('disabled', true);
	}
	if (!$('#on_off_to').prop('checked')) {
		$('#fieldMax_d').prop('disabled', true);
	}
	if ($('select[name=period_from_second] > option:selected').val() == 'now') {
		$('#fieldPeriodFrom').prop('disabled', true);
	}
	if ($('select[name=period_to_second] > option:selected').val() == 'now') {
		$('#fieldPeriodTo').prop('disabled', true);
	}
	$('#on_off_from').click(function(){
		if ($(this).prop('checked')) {
			$('#fieldMin_d').prop('disabled', false);
		} else {
			$('#fieldMin_d').prop('disabled', true);
		}
	});
	$('#on_off_to').click(function(){
		if ($(this).prop('checked')) {
			$('#fieldMax_d').prop('disabled', false);
		} else {
			$('#fieldMax_d').prop('disabled', true);
		}
	});
	$('select[name=period_from_second]').change(function(){
		if ($(this).find('option:selected').val() == 'now') {
			$('#fieldPeriodFrom').prop('disabled', true);
		} else {
			$('#fieldPeriodFrom').prop('disabled', false);
		}
	});

	$('select[name=period_to_second]').change(function(){
		if ($(this).find('option:selected').val() == 'now') {
			$('#fieldPeriodTo').prop('disabled', true);
		} else {
			$('#fieldPeriodTo').prop('disabled', false);
		}
	});
});
