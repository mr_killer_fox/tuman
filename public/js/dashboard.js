$(document).ready(function() {
	$('.js_dashboard').focus(function() {
		allPostDataForOne( $(this) );
	});
	$('.js_dashboard_team').focus(function() {
		var table = $(this).parents('.container').find('.js_data_team');
		$.ajax({
			url: '/../dashboard/team',
			type: 'POST',
			data: {
				start_date_team: $(this).parents('.row').find('.js_start_date_team').val(),
				end_date_team: $(this).parents('.row').find('.js_end_date_team').val()
			},
			success: function(data){
                table.html($($(data)).find('.js_dashboard_team').parents('.container').find('.js_data_team').html());
            },
		})
	});
	$('.js_dashboard').parents('.container').find('.js_dc_components').find('.js_data_graph').each(function ( index, value ) {
    	renderBar( dc.barChart("#js_graphic_"+index), d3.csv.parse(d3.select(value).text()) );
    });
	/*$('.js_select_line > .checkbox > input[type=checkbox]').click(function() {
		allPostDataForOne( $(this).parents('.container').find('.js_dashboard') );
	});*/
});

function allPostDataForOne( elem ) {
	var table = elem.parents('.container').find('.js_main_dasboard');
	/*var uri = '';
	$('.js_select_line > .checkbox > input[type=checkbox]').each(function(index){

		uri += '&kl_'+$(this).attr('name')+'='+$(this).prop('checked');

	});*/
	$.ajax({
		url: '/../dashboard/index'+elem.parents('.container').find('.js_url_for_ajax').val(),
		type: 'POST',
		data: '&start_date='+elem.parents('.row').find('.js_start_date').val()+'&end_date='+elem.parents('.row').find('.js_end_date').val(),
		success: function(data){
            table.html($($(data)).find('.js_dashboard').parents('.container').find('.col-md-8').html());
            table.parents('.container').find('.js_dc_components').find('.js_data_graph').each(function ( index, value ) {
            	$($(data)).find('.container').find('.js_dc_components').find('.js_data_graph').each(function ( index_res, value_res ) {
            		if (index === index_res) {
            			value = value_res;
            		}
            	});
            	renderBar( dc.barChart("#js_graphic_"+index), d3.csv.parse(d3.select(value).text()) );
            });
            //$('#js_data_graph').html($($(data)).find('#js_data_graph').html());
            
        },
	})
}

/*function coloredLinks() {

	if ($('.dc-legend-item').find('text:contains("Closed")').parent().find('rect').length > 0)
		$('#js_closed').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Closed")').parent().find('rect').attr('fill'));
	else
		$('#js_closed').parent().find('label').css("color", '#636c72');
	if ($('.dc-legend-item').find('text:contains("Bugs")').parent().find('rect').length > 0)
		$('#js_bugs').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Bugs")').parent().find('rect').attr('fill'));
	else
		$('#js_bugs').parent().find('label').css("color", '#636c72');
	if ($('.dc-legend-item').find('text:contains("Deadlines")').parent().find('rect').length > 0)
		$('#js_deadlines').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Deadlines")').parent().find('rect').attr('fill'));
	else
		$('#js_deadlines').parent().find('label').css("color", '#636c72');
	if ($('.dc-legend-item').find('text:contains("Prev. closed")').parent().find('rect').length > 0)
		$('#js_prev_closed').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Prev. closed")').parent().find('rect').attr('fill'));
	else
		$('#js_prev_closed').parent().find('label').css("color", '#636c72');
	if ($('.dc-legend-item').find('text:contains("Prev. bugs")').parent().find('rect').length > 0)
		$('#js_prev_bugs').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Prev. bugs")').parent().find('rect').attr('fill'));
	else
		$('#js_prev_bugs').parent().find('label').css("color", '#636c72');
	if ($('.dc-legend-item').find('text:contains("Prev. deadlines")').parent().find('rect').length > 0)
		$('#js_prev_deadlines').parent().find('label').css("color", $('.dc-legend-item').find('text:contains("Prev. deadlines")').parent().find('rect').attr('fill'));
	else
		$('#js_prev_deadlines').parent().find('label').css("color", '#636c72');
}

function renderGraph() {

	var arr_start_date = $('.js_start_date').val().split('/');
	var start_date = new Date(parseInt(arr_start_date[2]),parseInt(arr_start_date[0])-1,parseInt(arr_start_date[1]));
	var arr_end_date = $('.js_end_date').val().split('/');
	var end_date = new Date(parseInt(arr_end_date[2]),parseInt(arr_end_date[0])-1,parseInt(arr_end_date[1]));
	var format = d3.time.format("%Y-%m-%d");
	var chart = dc.seriesChart("#js_graphic");

	var experiments = d3.csv.parse(d3.select('pre#js_data_graph').text());

	experiments.forEach(function(x) {
		x.Values = +x.Values;
	});

	var ndx = crossfilter(experiments);
	var runDimension = ndx.dimension(function(d) {return [d.Ids, new Date(d.Dates)]; });
	var runGroup = runDimension.group().reduceSum(function(d) { return +d.Values; });

	chart
		.width(950)
		.height(350)
		//.chart(function(c) { return dc.lineChart(c).interpolate('cardinal'); })
		.x(d3.time.scale().domain([start_date,end_date]))
		//.y(d3.scale.linear().domain([0,10]))
		.brushOn(false)
		.yAxisLabel("Count")
		.xAxisLabel("Date")
		.clipPadding(10)
		.elasticY(true)
		.dimension(runDimension)
		.group(runGroup)
		.mouseZoomable(true)
		.seriesAccessor(function(d) {return d.key[0];})
		.keyAccessor(function(d) {return +d.key[1];})
		.title(function(d) { return d.key[1].getFullYear()+'-'+parseInt(d.key[1].getMonth()+1)+'-'+d.key[1].getDate() + ': ' + d.value; })
		.valueAccessor(function(d) {return +d.value;})
		.legend(dc.legend().x(750).y(0).itemHeight(13).gap(5).horizontal(1).legendWidth(140).itemWidth(70));

	chart.margins().left += 40;

	dc.renderAll();
}*/

function renderBar( chart, experiments ) {

	var arr_start_date = $('.js_start_date').val().split('/');
	var start_date = new Date(parseInt(arr_start_date[2]),parseInt(arr_start_date[0])-1,parseInt(arr_start_date[1]));
	var arr_end_date = $('.js_end_date').val().split('/');
	var end_date = new Date(parseInt(arr_end_date[2]),parseInt(arr_end_date[0])-1,parseInt(arr_end_date[1]));
	var format = d3.time.format("%Y-%m-%d");
	//var chart = dc.seriesChart("#js_graphic");

	//var experiments = d3.csv.parse(d3.select('pre#js_data_graph').text());

	experiments.forEach(function(x) {
		x.Values = +x.Values;
	});

	var ndx = crossfilter(experiments);
	var runDimension = ndx.dimension(function(d) { return new Date(d.Dates); });
	var runGroup = runDimension.group().reduceSum(function(d) { return d.Values; });

	chart
		.width(570)
		.height(250)
		.x(d3.time.scale().domain([start_date,end_date]))
		.brushOn(false)
		.yAxisLabel(experiments[0]['Ids'])
		.xAxisLabel("Date")
		.dimension(runDimension)
		.mouseZoomable(true)
		.group(runGroup)
		.title(function(d) { return d.key.getFullYear()+'-'+parseInt(d.key.getMonth()+1)+'-'+d.key.getDate() + ': ' + d.value; })
	
	chart.xUnits(function(){return 30;});
	chart.render();
}